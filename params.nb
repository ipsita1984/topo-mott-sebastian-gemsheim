(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     15658,        377]
NotebookOptionsPosition[     14011,        331]
NotebookOutlinePosition[     14347,        346]
CellTagsIndexPosition[     14304,        343]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"tc", "=", "0.12"}], ";", 
  RowBox[{"u", "=", "2.5"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Reduce", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"16", "v"}], "+", 
     RowBox[{"24", "*", "tc"}], "-", 
     RowBox[{"2", "u"}]}], "<=", "0"}], ",", "v"}], "]"}]}], "Input",
 CellChangeTimes->{{3.7291134199432783`*^9, 3.729113427823544*^9}, {
  3.729113463684037*^9, 3.7291134655248957`*^9}, {3.729113501705381*^9, 
  3.729113503170229*^9}, {3.729113604583323*^9, 3.7291136372734337`*^9}, {
  3.7291153875182343`*^9, 
  3.729115394650105*^9}},ExpressionUUID->"f87f7ccb-1907-45d7-bbbe-\
fcd6bd5c08a7"],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,44,12,30791960434786910409,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.7291134662953053`*^9, 3.729113503786701*^9, 3.729113638160692*^9, {
   3.7291153885801773`*^9, 
   3.72911539564323*^9}},ExpressionUUID->"ac7d72ae-a7e3-4c6a-9aef-\
b86483c5aaaa"],

Cell[BoxData[
 RowBox[{"v", "\[LessEqual]", "0.1325`"}]], "Output",
 CellChangeTimes->{
  3.729113429340893*^9, 3.7291134663123302`*^9, 3.7291135037937717`*^9, 
   3.729113638167781*^9, {3.729115388587756*^9, 
   3.7291153956557503`*^9}},ExpressionUUID->"0aa41e01-d22f-45d4-aa04-\
26b571032936"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"tc", "=", "0.5"}], ";", 
  RowBox[{"u", "=", "9"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"Reduce", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"16", "v"}], "+", 
     RowBox[{"24", "*", "tc"}], "-", 
     RowBox[{"2", "u"}]}], "<=", "0"}], ",", "v"}], "]"}]}], "Input",
 CellChangeTimes->{{3.729115506776636*^9, 
  3.7291155124737864`*^9}},ExpressionUUID->"8f3bfcc5-607e-4935-a6b3-\
8674197d0b70"],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,46,13,30791960434786910409,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.729115512807192*^9},ExpressionUUID->"580e4110-791f-486b-9e18-\
ffae6c557ed1"],

Cell[BoxData[
 RowBox[{"v", "\[LessEqual]", "0.375`"}]], "Output",
 CellChangeTimes->{
  3.729115512808079*^9},ExpressionUUID->"8bbdac8b-b8c5-44c9-b48a-\
0f7163653515"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"reg1", "=", 
  RowBox[{"RegionPlot", "[", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"16", "v"}], "+", 
      RowBox[{"24", "*", "0.5"}], "-", 
      RowBox[{"2", "u"}]}], "<=", "0"}], ",", 
    RowBox[{"{", 
     RowBox[{"u", ",", "0", ",", "12"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"v", ",", "0", ",", "1.2"}], "}"}], ",", " ", 
    RowBox[{"Frame", "\[Rule]", "True"}], ",", 
    RowBox[{"FrameLabel", "\[Rule]", 
     RowBox[{"{", 
      RowBox[{
      "\"\<U\>\"", ",", "\"\<V\>\"", ",", 
       "\"\<\!\(\*SubscriptBox[\(t\), \(c\)]\)=0.50\>\""}], "}"}]}], ",", 
    RowBox[{"FrameStyle", "\[Rule]", 
     RowBox[{"Directive", "[", 
      RowBox[{"Black", ",", "Bold", ",", "15"}], "]"}]}], ",", 
    RowBox[{"PlotLabel", "\[Rule]", 
     RowBox[{"Style", "[", 
      RowBox[{"\"\<\>\"", ",", "15", ",", "Bold"}], "]"}]}], ",", 
    RowBox[{"PlotLabel", "\[Rule]", 
     RowBox[{"Style", "[", 
      RowBox[{
      "\"\<\!\(\*SubscriptBox[\(t\), \(c\)]\)=0.50\>\"", ",", "15", ",", 
       "Bold"}], "]"}]}], ",", 
    RowBox[{"PlotStyle", "\[Rule]", "Red"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Export", "[", 
   RowBox[{"\"\<reg1.jpg\>\"", ",", "reg1"}], "]"}], ";"}]}], "Input",
 CellChangeTimes->{{3.729113846934844*^9, 3.729113933663948*^9}, {
  3.729113965689352*^9, 3.7291140045411663`*^9}, {3.7291141365737457`*^9, 
  3.729114177200367*^9}, {3.729114208484994*^9, 3.729114223891879*^9}, {
  3.729114300245226*^9, 3.729114348068408*^9}, {3.729114382794739*^9, 
  3.729114387570695*^9}, {3.729114425822465*^9, 3.729114501035635*^9}, {
  3.7291145322138166`*^9, 3.729114534716716*^9}, {3.7291146873066807`*^9, 
  3.729114688672489*^9}},ExpressionUUID->"c8067ed0-847a-4f5b-a594-\
e3331d48566d"],

Cell[BoxData[
 GraphicsBox[GraphicsComplexBox[CompressedData["
1:eJxllglQk9cWx1NEHqVGUYRKWEPIYmnqgrEq6r0tFlwok8pTrPry3EAoFlFT
ra9YBIQW6gKKSxWtcYm4IFpxbepCETHKoiARQdZEKSoqIig+mpcy/u83k8cM
w/zm++79n++c8z8H4YKl0yNseDzeU8vv33+vrM0OVQW60r4l/kvlHXETP0u6
80igd2N8Pbl/mkHpyfhQgchJH+PNODrw8nxZnpCxX+HcE6ntPoyfTHr9l1Hh
yzjv6tbPA1eLGS8LGpmt0UkYK66VtprN0v+Lp9bJZbun3wmCeMCIB4x4wIgH
jHjAiAeMeMCIx1p/QY56gKffZaYPhj4Y+mDog6EPhj4Y+mDoVwWpKo7RYqYP
hj4Y+mDog6EPhj4Y+tZ6B9v33/DwK2d6YOiBoQeGHhh6YOhZ319/ZYfcpKhk
94NxPxj3g3E/GPdb3+eauWHTMXqX3QfGfWDcB8Z91ufD5iU9XzGtlp0H4zwY
58E4f237b4c9/BrYeTDOg3He+v1F5tOzE4c3sffBeN/6+TuLT7xnUhjZc2ve
U3pENzngAUl+Z+xuXaqAlrXO0nr57SX/6LN83Gq+B1X+ap6dS8+R9bZHDYos
L+roEqZQT7tJ7HufC+mns/fEJw6/TfS9/etD44duqZscUPX2fRE98+oH6hh0
j4SmBEuyDL50240xSWXyRuJoZ1S3CyR0pP/fP82kInXtH0qVlJbvlBu3jTGx
+JbYyLZ2T3hIMM8QH+KFfzFPEC/it/Yv4kO8qC/6BfEifuv6Ij7Ei/zNo6HS
PM0QenpHXaHQL4P4XGn9hm9ypZPSPv3qOM0hxk9+KIyRuVGXZofXJkU+0fb6
zZ2mJ3bo1NN0ZIzItVSpEtLjL53tto+5Q3YuXPzSbBbSqf9yPNs9wUC69592
z9P40JZChyhVYDWZa+wzSRUoous+7OtaMLmGUMlP9voYCW3hPe2acNZI9kVW
D1vNl9Kzo5aUPz1vIraHpOGyPClNifozR6Pj4sU8Rn2zV0UvMykK2TwCR/Xm
z4OOrV/oMyWgiNXz2bX4fQZlHetvsM53egzfJKKX3rz+T1pYPTtvHrD0cuLw
AvbcJ8t8XRx6n/UX5tEHvfXwpCuOz1rnGKRn/Yb+w7wCr9wXn9Au8Ka5az5/
YFCWsvcxb+426bUanTdVS4QHyuS3WP3Rv+gPMOrhuNFthr9/Bfte+NEzYu9m
XaovPT6ie+vKYw2sn9Hf8Cv4+4Nt52NkYromYlWsLI/rf/i1wTS+UaAX02k/
dwTdO9nE+gv+QP+BUW9ByTKv9fnN7PuRL8SP74U+4sV56CH/1n6Hf1AP1A/5
sPY3/IL8oL6oB/oJjHqjPogf9UF9wagn/IHvQz7Rf2DUC/nF9yO/qA8Y9YB/
kJ/NSbvCAh25/Qx/2GW+7zEl4DqZ9LU8/A8Hbn+iP9ff6Cn2OHqL7KgK0Gr4
3L5TFhLjTmeun+Cf6a1DdYXRDSQk7pfM8UO4ec/2+aMFXyY+ayJ7q3MbBG7c
fC+6//EWw6YhVJ7jdq35RSaJfFYZmVMqoPseh7TOV58hsiN9B+kt8zCqO+NS
06hCYlPxe15qiBfluU1tMQwvIW09F7Saq9705u7EHyNdKkjrc75txAwh1RaN
lYhm3CHjv6v9ukTtQ6cGz1nUc7uauN9VB6os83FmWsaUmZZ8//hYuVl3ypdW
rO+MlS1uJJ08eaOgQky7Kn3Pi7XNpL0jYWLdAku//8mzX3fVSIK+D80JSpLS
04cHSpfZPmTzvqGtckt3z0Nye9ndj17v4ea/96EtBiWPm1fYBxlOE0synA+Q
9IShrnobbj+4mKePzOrL7Qfsi+oByTUXxPlsX7Q6L1bm0otkraFlBd+e2x/9
wgdtMDi4s/7HPvlrw8GmMnkh8bS5uoTvxO1D+KV/Vlp4weRS0k9esFC23pvN
Eza/SkZH3htRyeZNVunSPtvPVZGbBXbBdRu5/ZTV22/cfmL7qjxnbxipZvtq
963GCY5FNaQ4NrIjIZPbXxkfveGZBnH7C/tMWCGouRlSR4asq3xm3sbtY/jX
JabIQZTbSAYPH7VH87OYzRv4aecS86F6fyObR3Of7OqcsMpEKq9+mxieze3H
7BT+Rp0Htx/hL8/YMZ9lX3jA6llWlb5nuz+PjGx3alLkudLJzjxF7fMjpNL2
5Gm7Njc6jm/nFB53iWy+2DhPJvek+elPwu7/WkzOvAhJaFd4U+2MsInRhnLm
r295KamRoyuJz3+/8xtsEtIb/aPivLMMZHnK1K3xtiIatyntePfAWjK9qekr
vq8vvRhVf/1wcAO52HnxfEygmJpO8AcfSuX8FZoqOScebSQfmJ9EbGiX0JX8
Ibk2JhPzE+YZuLzjE1HL6EJS2nC4RLnbg8oSjy6Yry5ifsF8AqfbRb+Z9UUd
iX0RpBHWWv5fuumxyzmunox7meKep/Ok6oH2XXPa9W+fe1F1Uludc1wZaUse
+OrSGm+qSjs41r6Lmy9ByccqF1rmi7Ynv7g8xpfGOmiXpxkayOV45/fS3/jS
XO0vj7s6Lf5MTR459icxvaI/k7nSpZl9b37wuH8fsMyTV2XTvuETAdV52PaY
Xp4k5a/6OOnXuNOVwwKkLZ1XiO2j91+aP/Sx+O/daNWpu2S5+UWgKkRE6/ZH
9MwacZ/0a0sbtnqclNZsijqsUT8gk151zZTppfTJqYRQ76yHJP23pZI8L1ca
3L01p/GFhiwySgtjVG70nxN/F9c8v/A2f0K65MY9z/nqqrf5sPT3nI9PzWm/
R6qa3ez1RyS0OOYLW9EME/kf6SjWwg==
   "], {{{
      {RGBColor[1, 0, 0], AbsoluteThickness[1.6], Opacity[1], EdgeForm[None], 
       GraphicsGroupBox[{PolygonBox[CompressedData["
1:eJwBPgPB/CFib1JiAgAAAA8BAAADAAAARAILDQMEEwkKDgQFEAYHEQcIDAID
EggJRQwUZBUdFw4PhA0VDwUGGA8QGxITGRARGhESGCEgihYeIBcYIRgZIhka
Jx8gKCAhbh4ljR8mIxobFg0OKSEiLicogXRHcyYsHxYXKiIjLygpMCkqLSYn
SCwxMiwtMy0uSTI2AgwLNS8whzM3Zzc7ODM0OTQ1Aw0MkDg8gnlKkz1Ag35L
PTg5fUBCWU5EeDw/TEJDgG9GBA4NERoZBxEQDhcWBhAPCBIRhBQMCRMSW1BF
BQ8ODRYVNC4vEhsaEBkYDxgXZWQcFyAfih0VFh8eGSIhGiMib24kjSUeHycm
Ji0sJy4tIiopdHMrICgnISkoKC8uLjQzXVJILDIxhzYyKTAvLzU0X1RJLTMy
d3ZKfHtLaGc6kDs3OD08AkQBT0QLfn1BYVZMeXg+Mzg3DEULU0gxbWxGcnFH
NDk4UUUUMkkxVUk2LEgrcW4lQkxBbGQdRE4BWk8LSVQxa1U2SFIrXlMxRVAL
alEUTFZBUVtFTlgBT1lEVF4xYldDUlwrU11IUFoLcG8kV0xDV2FMTWJDWGMB
iYhndmc7ZmUchmUUhoVkVV9JaWg6gGwdZWoUaGs2VmBBiWg2bG0cjItuZGwc
kz88jG8d571TdHVHcXIkK3MsdXQrj45zgXElbnEkj3QlZ3Y6gnY7e3g/pdBc
knk7enk+eHs+eXpKg3s/lZR9foM/f35BcYFHQX1ClX4/bIBGfn9Lb4AdkpF4
dIEle3w+e4NLdoJKeYI7DYQMZIUVhYQVhIUUv+5Udnc6ZYZkZ4g3dI9ziIk2
FooVi4oeh4g2iosdbosei4wdH40ejo0maIlnjY4ljo8lb4xuc44mOJA3kZA8
kJE7lJU/eZJ4kZI7lJNAPZM8M4cyk5Q/hYYUeJE8fpV9uqRHxKhKfZRAuKNG
oMtYiIc3xqlLqtVgx+9Wu+1S6clXtuZRntQ+r+tOs+xQrOpN7sBUsuVPzpYc
b3BG7bxS5rVR7LRQ5bFP6q1NvudTo7dG78hWyulX67BO05s6qMNK3NtpnNM6
mc8kpLlHwuhV1J0+6MFVz5gkl84cqcVL2dhmJ14Oug==
          "]], 
         PolygonBox[{{210, 191, 84, 95}, {209, 187, 82, 93}, {183, 221, 109, 
          70}, {162, 182, 81, 106}, {161, 178, 79, 90}, {185, 223, 114, 71}, {
          180, 161, 90, 80}, {150, 217, 102, 28}, {155, 220, 105, 58}, {157, 
          226, 122, 62}, {176, 160, 88, 78}, {216, 215, 101, 102}, {177, 204, 
          89, 79}, {214, 199, 86, 97}, {154, 224, 117, 43}, {152, 222, 112, 
          36}, {159, 228, 127, 65}, {213, 159, 65, 96}, {181, 205, 91, 81}, {
          219, 218, 104, 105}, {193, 210, 95, 85}, {189, 209, 93, 83}, {215, 
          162, 106, 101}, {192, 166, 94, 84}, {221, 151, 28, 109}, {208, 154, 
          43, 92}, {173, 171, 98, 77}, {166, 190, 83, 94}, {201, 214, 97, 
          87}, {203, 174, 99, 88}, {188, 165, 92, 82}, {171, 202, 87, 98}, {
          218, 167, 107, 104}, {222, 184, 70, 112}, {195, 225, 119, 74}, {205,
           179, 80, 91}, {204, 175, 78, 89}, {197, 227, 124, 75}, {228, 198, 
          75, 127}, {200, 170, 96, 86}, {227, 158, 62, 124}, {223, 153, 36, 
          114}, {224, 186, 71, 117}, {226, 196, 74, 122}, {225, 156, 58, 
          119}, {167, 194, 85, 107}}]}]}}, {}, {}, {}, {}}, {{
      {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], 
       LineBox[{2, 1, 99, 174, 203, 160, 176, 235, 175, 204, 177, 229, 178, 
        161, 180, 236, 179, 205, 181, 230, 182, 162, 215, 216, 217, 150, 206, 
        151, 221, 183, 163, 184, 222, 152, 207, 153, 223, 185, 164, 186, 224, 
        154, 208, 165, 188, 237, 187, 209, 189, 231, 190, 166, 192, 238, 191, 
        210, 193, 232, 194, 167, 218, 219, 220, 155, 211, 156, 225, 195, 168, 
        196, 226, 157, 212, 158, 227, 197, 169, 198, 228, 159, 213, 170, 200, 
        239, 199, 214, 201, 233, 202, 171, 173, 234, 172, 77, 67, 66, 64, 61, 
        57, 53, 48, 42, 35, 27, 19, 10, 9, 8, 7, 6, 5, 4, 3, 2}]}}}}],
  AspectRatio->1,
  Axes->{False, False},
  AxesLabel->{None, None},
  AxesOrigin->{Automatic, Automatic},
  DisplayFunction->Identity,
  Frame->{{True, True}, {True, True}},
  FrameLabel->{{
     FormBox["\"V\"", TraditionalForm], None}, {
     FormBox["\"U\"", TraditionalForm], 
     FormBox[
     "\"\\!\\(\\*SubscriptBox[\\(t\\), \\(c\\)]\\)=0.50\"", TraditionalForm]}},
  FrameStyle->Directive[
    GrayLevel[0], Bold, 15],
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "ScalingFunctions" -> None, "TransparentPolygonMesh" -> True, 
    "AxesInFront" -> True},
  PlotLabel->FormBox[
    StyleBox["\"\"", 15, Bold, StripOnInput -> False], TraditionalForm],
  PlotRange->{{0, 12}, {0, 1.2}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.02]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.729114453242586*^9, {3.729114484351207*^9, 3.7291145019712152`*^9}, 
   3.729114536133376*^9, 
   3.729114689240333*^9},ExpressionUUID->"d9993b1c-df6c-4cbd-a62c-\
e40c47f8561d"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.729115705020846*^9, 
  3.729115738656348*^9}},ExpressionUUID->"9a024d74-ae57-4f1a-9c8f-\
0c62c72bab41"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"tc", "=", "1"}], ";", 
  RowBox[{"u", "=", "10"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"24", "*", "tc"}], "-", 
  RowBox[{"2.0", "u"}]}], "\[IndentingNewLine]", 
 RowBox[{"Reduce", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"16", "v"}], "+", 
     RowBox[{"24", "*", "tc"}], "-", 
     RowBox[{"2.0", "u"}]}], "<=", "0"}], ",", "v"}], "]"}]}], "Input",
 CellChangeTimes->{{3.729115506776636*^9, 3.7291155124737864`*^9}, {
  3.729115753684263*^9, 3.72911576421451*^9}, {3.7291161826966753`*^9, 
  3.72911620206149*^9}},ExpressionUUID->"067cf7a4-b172-4eef-81fd-\
60bdde424e87"],

Cell[BoxData["4.`"], "Output",
 CellChangeTimes->{{3.729115759022481*^9, 3.729115765073943*^9}, {
  3.729116185030128*^9, 
  3.729116202731133*^9}},ExpressionUUID->"57031784-288a-44e0-a724-\
0ffd4bf25f9a"],

Cell[BoxData[
 TemplateBox[{
  "Reduce","ratnz",
   "\"Reduce was unable to solve the system with inexact coefficients. The \
answer was obtained by solving a corresponding exact system and numericizing \
the result.\"",2,56,16,30791960434786910409,"Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{
  3.729115765060326*^9, {3.729116185031541*^9, 
   3.729116202732485*^9}},ExpressionUUID->"6350169b-0c65-48a2-aada-\
6549e004f570"],

Cell[BoxData[
 RowBox[{"v", "\[LessEqual]", 
  RowBox[{"-", "0.25`"}]}]], "Output",
 CellChangeTimes->{{3.729115759022481*^9, 3.729115765073943*^9}, {
  3.729116185030128*^9, 
  3.729116202741912*^9}},ExpressionUUID->"9196460c-c050-4c67-8298-\
efc09fd24c91"]
}, Open  ]]
},
WindowSize->{808, 621},
WindowMargins->{{195, Automatic}, {59, Automatic}},
FrontEndVersion->"11.1 for Linux x86 (64-bit) (March 13, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 649, 16, 59, "Input", "ExpressionUUID" -> \
"f87f7ccb-1907-45d7-bbbe-fcd6bd5c08a7"],
Cell[1232, 40, 502, 11, 41, "Message", "ExpressionUUID" -> \
"ac7d72ae-a7e3-4c6a-9aef-b86483c5aaaa"],
Cell[1737, 53, 295, 6, 33, "Output", "ExpressionUUID" -> \
"0aa41e01-d22f-45d4-aa04-26b571032936"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2069, 64, 447, 13, 59, "Input", "ExpressionUUID" -> \
"8f3bfcc5-607e-4935-a6b3-8674197d0b70"],
Cell[2519, 79, 401, 9, 41, "Message", "ExpressionUUID" -> \
"580e4110-791f-486b-9e18-ffae6c557ed1"],
Cell[2923, 90, 168, 4, 33, "Output", "ExpressionUUID" -> \
"8bbdac8b-b8c5-44c9-b48a-0f7163653515"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3128, 99, 1897, 45, 155, "Input", "ExpressionUUID" -> \
"c8067ed0-847a-4f5b-a594-e3331d48566d"],
Cell[5028, 146, 7214, 129, 401, "Output", "ExpressionUUID" -> \
"d9993b1c-df6c-4cbd-a62c-e40c47f8561d"]
}, Open  ]],
Cell[12257, 278, 152, 3, 33, "Input", "ExpressionUUID" -> \
"9a024d74-ae57-4f1a-9c8f-0c62c72bab41"],
Cell[CellGroupData[{
Cell[12434, 285, 638, 17, 82, "Input", "ExpressionUUID" -> \
"067cf7a4-b172-4eef-81fd-60bdde424e87"],
Cell[13075, 304, 205, 4, 33, "Output", "ExpressionUUID" -> \
"57031784-288a-44e0-a724-0ffd4bf25f9a"],
Cell[13283, 310, 451, 10, 41, "Message", "ExpressionUUID" -> \
"6350169b-0c65-48a2-aada-6549e004f570"],
Cell[13737, 322, 258, 6, 33, "Output", "ExpressionUUID" -> \
"9196460c-c050-4c67-8298-efc09fd24c91"]
}, Open  ]]
}
]
*)

