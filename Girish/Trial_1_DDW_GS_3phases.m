% script following Free energy minimization expression 
% finding DDW/SDW/DSC/SDW order here for fixed U
clc
clear all
tic

% parameters
t=1;
tdiag=0; % diagonal hopping
Vvals=10;
VV=linspace(0,.030,Vvals);
U=0.2*t;

muvals=10;
MU=linspace(0,1.0,muvals);

Mvals=100;
MM=linspace(0,.10,Mvals);

PH1=zeros(Vvals,muvals);
PH3=zeros(Vvals,muvals);

FE1=nan(Vvals,muvals);
FE3=nan(Vvals,muvals);

PHASE=zeros(Vvals,muvals);

for Vval=1:Vvals
   
    V=VV(Vval);
    tc=5*V;
    for muval=1:muvals
        mu=MU(muval);
        for Mval=1:Mvals
            M=MM(Mval);
            
            %singlet DDW
            Val1(Mval)=MF_integral_1p1_DDW_GS(M,t,tdiag,tc,mu,V,U,0);
            % SDW
            Val3(Mval)=MF_integral_2_DDW_GS(M,t,tdiag,tc,mu,V,U,0);
        end
        Diff1=(Val1-MM);Diff3=(Val3-MM);
        for I=3:Mvals
            if sign(Diff1(I))~=sign(Diff1(I-1))
                PH1(Vval,muval)=(MM(I));
                FE1(Vval,muval)=Free_energy_1p1_GS(M,t,tdiag,tc,mu,V,U,0);
                break;
            end
        end
        for I=3:Mvals
            if sign(Diff3(I))~=sign(Diff3(I-1))
                PH3(Vval,muval)=(MM(I));
                FE3(Vval,muval)=Free_energy_2_GS(M,t,tdiag,tc,mu,V,U,0);
                break;
            end
        end
        
        [val,ind]=min([FE1(Vval,muval),FE3(Vval,muval)]);
        
        if isnan(val)==0
            if ind==1
                PHASE(Vval,muval)=1;
            end
            if ind==2
                PHASE(Vval,muval)=2;
            end
          
        end
                
        
    end
end


  
  
  
fid = fopen('PHASE.txt','wt');
for ii = 1:size(PHASE,1)
    fprintf(fid,'%g\t',PHASE(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)



fid = fopen('VV.txt','wt');
for ii = 1:size(VV,1)
    fprintf(fid,'%g\t',VV(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)



fid = fopen('MU.txt','wt');
for ii = 1:size(MU,1)
    fprintf(fid,'%g\t',MU(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

toc



  
  
  
  
  
  