function val = MF_integral_3p1_DDW_GS(del,t,tdiag,tc,mu,V,U,T)

% this is for DSC order with accounting for tdiag

N=100;
K=linspace(0,pi,N);

g=12*tc - 8*V;
Int=zeros(N,N);
for I=1:N
    for J=1:N
        kx=K(I);ky=K(J);
        if (ky+kx) <pi 
            
            f=cos(kx)-cos(ky);
            ek=-2*t*(cos(kx)+cos(ky));
            ekb=-2*tdiag*sin(kx)*sin(ky);
            
            E1=abs( sqrt( (f*del)^2 + ek^2 + ekb^2 + mu^2 + 2*mu*sqrt(ek^2+ekb^2) ));
            E2=abs( sqrt( (f*del)^2 + ek^2 + ekb^2 + mu^2 - 2*mu*sqrt(ek^2+ekb^2) ));
            E3=-E1;E4=-E2;
            
            if mu>E1 
                Th1=1; 
            else
                Th1=0;
            end
            if mu>E2 
                Th2=1; 
            else
                Th2=0;
            end
            if mu>E3 
                Th3=1; 
            else
                Th3=0;
            end
            if mu>E4 
                Th4=1; 
            else
                Th4=0;
            end

            
            Int(I,J) = (f*f*del*Th1/E1)+ (f*f*del*Th2/E2) +(f*f*del*Th3/E3)+ (f*f*del*Th4/E4);  
            
        end
        
    end
end
Int=Int*(-g/2);
val=( trapz(K,trapz(K,Int)) );
val=val/(pi*pi/2);


end