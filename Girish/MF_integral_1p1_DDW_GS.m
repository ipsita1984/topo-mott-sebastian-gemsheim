function val = MF_integral_1p1_DDW_GS(del,t,tdiag,tc,mu,V,U,T)


% this is for  triplet DDW 
N=100;
K=linspace(0,pi,N);

g=8*V + 24*tc;
Int=zeros(N,N);
for I=1:N
    for J=1:N
        kx=K(I);ky=K(J);
        if (ky+kx) <pi 
            f=cos(kx)-cos(ky);
            ek=-2*t*(cos(kx)+cos(ky));
            ekb=-2*tdiag*sin(kx)*sin(ky);
            
            Ek=abs(sqrt((ek*ek) + (del*del*f*f) + (ekb*ekb) ));
            E1=-Ek;
            E2=Ek;
            
            if mu>E1 
                Th1=1; 
            else
                Th1=0;
            end
            if mu>E2 
                Th2=1; 
            else
                Th2=0;
            end
            Int(I,J) = ((f*f*del*Th1/E1) + (f*f*del*Th2/E2)); 
            
            
        end
        
    end
end

Int=Int*(-g);



val=( trapz(K,trapz(K,Int)) );
val=val/(pi*pi/2);


end