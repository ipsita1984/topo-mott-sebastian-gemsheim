function val = Free_energy_2_GS(del,t,tdiag,tc,mu,V,U,T)

% this is for SDW order 

N=100;
K=linspace(0,pi,N);

g=2*U;
Int=zeros(N,N);
for I=1:N
    for J=1:N
        kx=K(I);ky=K(J);
        if (ky+kx) <pi 
            
            ek=-2*t*(cos(kx)+cos(ky));
            ekb=-2*tdiag*sin(kx)*sin(ky);
            E1=abs(sqrt((ek*ek) + ((ekb+del)^2)));
            E2=abs(sqrt((ek*ek) + ((ekb-del)^2)));
            E3=-E1;
            E4=-E2;
            
            if mu>E1 
                Th1=1; 
            else
                Th1=0;
            end
            if mu>E2 
                Th2=1; 
            else
                Th2=0;
            end
            if mu>E3 
                Th3=1; 
            else
                Th3=0;
            end
            if mu>E4 
                Th4=1; 
            else
                Th4=0;
            end
            
            Int(I,J) = (E1*Th1) + (E2*Th2) +(E3*Th3) + (E4*Th4); 
            
        end
        
    end
end

val=( trapz(K,trapz(K,Int)) );
val=val/(pi*pi/2);

val = val + del*del/g;

end