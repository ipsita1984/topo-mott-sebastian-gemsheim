







VV_data = importdata('VV.txt');
MU_data = importdata('MU.txt');
PHASE_data = importdata('PHASE.txt');



figure
x0=10;
y0=10;
width=600;
height=500;
set(gcf,'units','points','position',[x0,y0,width,height])
set(gca,'fontsize',32)
pcolor(MU_data,VV_data,(PHASE_data))
xlabel('\mu/t','fontsize',32)
ylabel('V/t','fontsize',32)
title('t = 1,t_c = 5V, U = 0.2t : (Yellow color~SDW, Cyan Green color~DDW)')
colorbar
print('Phase_diagram','-dpdf')

%shading interp
%xlim([-2.6 2.6])
%ylim([0 3])
%print("plot.pdf")    %showing error!!!
  % Write this to file.