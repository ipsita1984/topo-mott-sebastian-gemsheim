import numpy as np
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
from scipy import integrate, LowLevelCallable
import sys
from Free_energy2 import *
import ctypes
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

### number of grid points for "manual integration" of kx and ky 
Ngrid = 50

### Integration options
options = {'limit' : 100, "epsrel" : 1e-5, 'epsabs' : 1e-5, 'maxp1' : 100}



def main():
	
	### PARAMETER
	t = 1.0
	tc = 0.05	#2
	V = 0.0	#1
	U = 1.0 
	tdiag = 0.75
	T = 0.0
	mu = 0.0

	ga = 2.0 * U
	gb = 8.0 * V + 24.0*tc
	print 'ga = ', ga,' gb = ', gb
	
	lwg = 2.0
	
	Nd = 100
	phimin = 0.0
	phimax = 1.5
	phisa = np.linspace(phimin, phimax, Nd)
	phisb = np.linspace(phimin, phimax, Nd)
	
	if True:
		fac = np.zeros(Nd)
		fdac = np.zeros(Nd)
		fbc = np.zeros(Nd)
		fdbc = np.zeros(Nd)
		start = time.time()
		for n in np.arange(Nd):
			print n+1, " / ", Nd
			fac[n] = Free_energy_C_a2(phisa[n], t, tdiag, tc, mu, V, U, T)
			fbc[n] = Free_energy_C_b2(phisb[n], t, tdiag, tc, mu, V, U, T)
		print 'mean(time) = %.4f s' %((time.time() - start)/Nd)
		
		# get minimum phi values
		minphia = phisa[np.argmin(fac)]
		minphib = phisb[np.argmin(fbc)]
		minfa = np.min(fac)
		minfb = np.min(fbc)
		if minfa < minfb:
			minPhase = 'a'
		else:
			minPhase = 'b'
		print 'min phi a:', minphia
		print 'min phi b:', minphib
		
		if True:
			# show free energy curve for minimizations
			plt.figure(figsize=(10,8))
			plt.suptitle(r"$U=%.3f$, $V=%.3f$, $\mu=%.3f$, $t=%.3f$, $t_{diag}=%.3f$, $t_c=%.3f$" %(U,V,mu,t,tdiag,tc))
			plt.title(r"Free energy --- $F(\phi_a,\phi_b=0)$ and $F(\phi_a=0,\phi_b)$")
			plt.plot(phisa, fac, label=r'$F(\phi_a,\phi_b=0)$')
			plt.axvline(phisa[np.argmin(fac)],color='blue')
			plt.axhline(np.min(fac),color='blue')
			plt.plot(phisa, fbc, label=r'$F(\phi_a=0,\phi_b)$')
			plt.axvline(phisb[np.argmin(fbc)],color='orange')
			plt.axhline(np.min(fbc),color='orange')
			
			plt.xlabel(r"$2\phi/g$")
			plt.ylabel(r"$F$")
			plt.legend(loc='best')
			plt.grid()
			plt.tight_layout(rect=[0,0,1,0.94])
			plt.show()
			
	if False:
		# 2D plots of Ek's
		ks1 = np.linspace(-np.pi, 0.0, 50)
		ks2 = np.linspace(0.0, np.pi, 50)
		boundary11 = -np.pi - ks1
		boundary12 = np.pi + ks1
		boundary21 = np.pi - ks2
		boundary22 = -np.pi + ks2
		
		if minPhase == 'a':
			Fk_a1 = Free_energy_a1_k(minphia, t, tdiag, tc, mu, V, U, T, N=Ngrid)
			Fk_a2 = Free_energy_a2_k(minphia, t, tdiag, tc, mu, V, U, T, N=Ngrid)
			plt.figure(figsize=(12,7))
			plt.suptitle(r"$U=%.3f$, $V=%.3f$, $\mu=%.3f$, $t=%.3f$, $t_{diag}=%.3f$, $t_c=%.3f$" %(U,V,mu,t,tdiag,tc))
			plt.subplot(121)
			plt.title(r"$E^1(\phi^{min}_a,kx,ky)$")
			plt.imshow(Fk_a1, interpolation='none', origin='lower', aspect='auto', extent=[-np.pi,np.pi,-np.pi,np.pi])
			plt.plot(ks1,boundary11,lw=2,color='black',ls='--')
			plt.plot(ks1,boundary12,lw=2,color='black',ls='--')
			plt.plot(ks2,boundary21,lw=2,color='black',ls='--')
			plt.plot(ks2,boundary22,lw=2,color='black',ls='--')
			plt.axhline(0.0,lw=1,color='black',ls='--')
			plt.axvline(0.0,lw=1,color='black',ls='--')
			plt.colorbar()
			plt.subplot(122)
			plt.title(r"$E^2(\phi^{min}_a,\phi_b=0,kx,ky)$")
			plt.imshow(Fk_a2, interpolation='none', origin='lower', aspect='auto', extent=[-np.pi,np.pi,-np.pi,np.pi])
			plt.plot(ks1,boundary11,lw=2,color='black',ls='--')
			plt.plot(ks1,boundary12,lw=2,color='black',ls='--')
			plt.plot(ks2,boundary21,lw=2,color='black',ls='--')
			plt.plot(ks2,boundary22,lw=2,color='black',ls='--')
			plt.axhline(0.0,lw=1,color='black',ls='--')
			plt.axvline(0.0,lw=1,color='black',ls='--')
			plt.colorbar()
			plt.tight_layout(rect=[0,0,1,0.94])
			plt.show()
		
		else:
			Fk_b1 = Free_energy_b1_k(minphia, t, tdiag, tc, mu, V, U, T, N=Ngrid)
		
			plt.figure(figsize=(9,7))
			plt.suptitle(r"$U=%.3f$, $V=%.3f$, $\mu=%.3f$, $t=%.3f$, $t_{diag}=%.3f$, $t_c=%.3f$" %(U,V,mu,t,tdiag,tc))
			plt.title(r"$E^1(\phi_a=0,\phi^{min}_b,kx,ky)$")
			plt.imshow(Fk_b1, interpolation='none', origin='lower', aspect='auto', extent=[-np.pi,np.pi,-np.pi,np.pi])
			plt.plot(ks1,boundary11,lw=2,color='black',ls='--')
			plt.plot(ks1,boundary12,lw=2,color='black',ls='--')
			plt.plot(ks2,boundary21,lw=2,color='black',ls='--')
			plt.plot(ks2,boundary22,lw=2,color='black',ls='--')
			plt.axhline(0.0,lw=1,color='black',ls='--')
			plt.axvline(0.0,lw=1,color='black',ls='--')
			plt.colorbar()
			plt.tight_layout(rect=[0,0,1,0.94])
			plt.show()
	
	
	fs_label = 22#18
	fs_ticks = 20#16
	fs_legend = fs_label
	
	lw = 2
	
	nks = 500
	
	if True and minPhase == 'a':
		
		ks1 = np.linspace(-np.pi, 0.0, nks)
		ks2 = np.linspace(0.0, np.pi, nks)
		boundary11 = -np.pi - ks1
		boundary12 = np.pi + ks1
		boundary21 = np.pi - ks2
		boundary22 = -np.pi + ks2
		
		K = np.linspace(-np.pi, np.pi, nks)
		mkx, mky = np.meshgrid(K, K, indexing='ij', copy=False)
		
		Fk_a1 = Free_energy_a1_k(minphia, t, tdiag, tc, mu, V, U, T, N=nks)
		Fk_a2 = Free_energy_a2_k(minphia, t, tdiag, tc, mu, V, U, T, N=nks)
		
		fmax = np.max(np.abs(Fk_a1))
		
		fig = plt.figure(figsize=(10,7))
		ax = Axes3D(fig)
		ax.plot_surface(mkx, mky, Fk_a1, rstride=1, cstride=1, cmap='seismic',vmin=-fmax,vmax=fmax,alpha=0.5)
		ax.plot_surface(mkx, mky, Fk_a2, rstride=1, cstride=1, cmap='PuOr_r',vmin=-fmax,vmax=fmax)
		ax.plot_surface(mkx, mky, -Fk_a1, rstride=1, cstride=1, cmap='seismic',vmin=-fmax,vmax=fmax, alpha=0.5)
		ax.plot_surface(mkx, mky, -Fk_a2, rstride=1, cstride=1, cmap='PuOr_r',vmin=-fmax,vmax=fmax)
		ax.set_xlabel(r"$k_x$",fontsize=fs_label, labelpad=18)
		ax.set_ylabel(r"$k_y$",fontsize=fs_label, labelpad=18)
		ax.set_zlabel("Energy", fontsize=fs_label, labelpad=10)
		ax.view_init(elev=10,azim=35)
		
		
		ax.tick_params(direction='in', which='both', labelsize=fs_ticks, top=False, left=False, right=False, bottom=False)
		
		ax.xaxis.set_major_locator(plt.MaxNLocator(5))
		ax.yaxis.set_major_locator(plt.MaxNLocator(5))
		ax.zaxis.set_major_locator(plt.MaxNLocator(5))
		
		# z label rotation
		ax.zaxis.set_rotate_label(False)
		a = ax.zaxis.label.get_rotation()
		print a
		if a<180:
			a += 90#180
		ax.zaxis.label.set_rotation(a)
		
		plt.savefig(r"surfaceE_%s.png" %(labelName(U,V,mu,t,tdiag,tc)), dpi=200)
		plt.savefig(r"surfaceE_%s.pdf" %(labelName(U,V,mu,t,tdiag,tc)))
		#plt.show()
		plt.close()
		
		
		if True:
			### plot GammaXM Scheme
			Npa = nks * 4
			Npa2 = np.int(np.sqrt(2)*Npa)
			GXMGpath_kx = []
			GXMGpath_ky = []
			### G -> X
			for kx in np.linspace(0.0, -np.pi/2,Npa):
				GXMGpath_kx.append(kx)
			for ky in np.linspace(0.0, np.pi/2,Npa):
				GXMGpath_ky.append(ky)
			### X -> M
			for kx in np.linspace(-np.pi/2, 0.0,Npa):
				GXMGpath_kx.append(kx)
			for ky in np.linspace(np.pi/2,np.pi,Npa):
				GXMGpath_ky.append(ky)
			### M -> G
			for kx in np.zeros(Npa2):
				GXMGpath_kx.append(kx)
			for ky in np.linspace(np.pi,0.0,Npa2):
				GXMGpath_ky.append(ky)
			
			GXMGpath_kx = np.array(GXMGpath_kx)
			GXMGpath_ky = np.array(GXMGpath_ky)
			
			pathE1 = Free_energy_a1_k_custom(minphia, GXMGpath_kx, GXMGpath_ky, t, tdiag, tc, mu, V, U, T, N=nks)
			pathE2 = Free_energy_a2_k_custom(minphia, GXMGpath_kx, GXMGpath_ky, t, tdiag, tc, mu, V, U, T, N=nks)
			
			plt.figure(figsize=(9,5))
			plt.plot(np.arange(len(pathE1)), pathE1, label=r'$E^1_k$',lw=lwg)
			plt.plot(np.arange(len(pathE1)), -pathE1, label=r'$E^3_k$',lw=lwg)
			plt.plot(np.arange(len(pathE1)), pathE2, label=r'$E^2_k$',lw=lwg)
			plt.plot(np.arange(len(pathE1)), -pathE2, label=r'$E^4_k$',lw=lwg)
			
			plt.axvline(0,ls='--',color='black',lw=lw)
			plt.axvline(Npa-1,ls='--',color='black',lw=lw)
			plt.axvline(2*Npa-1,ls='--',color='black',lw=lw)
			plt.axvline(2*Npa+Npa2-1,ls='--',color='black',lw=lw)
			plt.xticks([0,Npa-1,2*Npa-1,2*Npa+Npa2-1],(r"$\Gamma$",r"$X$",r"$M$",r"$\Gamma$"))
			plt.ylabel("Energy", fontsize=fs_label)
			plt.grid()
			
			ax = plt.gca()
			ax.tick_params(direction='in', which='both', labelsize=fs_ticks, top=False, left=False, right=False, bottom=False)

			ax.yaxis.set_major_locator(plt.MaxNLocator(5))
			
			
			plt.legend(fontsize=fs_legend, loc='center right')
			
			plt.tight_layout(pad=0.0)
			
			
			### inset
			if False:
				ax = plt.gca()
				axins = zoomed_inset_axes(ax, 15.0, loc='lower center')
				plt.plot(np.arange(len(pathE1)), pathE1, label=r'$E^1_k$',lw=lwg)
				plt.plot(np.arange(len(pathE1)), -pathE1, label=r'$E^3_k$',lw=lwg)
				plt.plot(np.arange(len(pathE1)), pathE2, label=r'$E^2_k$',lw=lwg)
				plt.plot(np.arange(len(pathE1)), -pathE2, label=r'$E^4_k$',lw=lwg)
				
				mark_inset(ax, axins, loc1=2, loc2=3, fc="none", ec="0.5")
				
				xmid = np.argmin(pathE2)
				x1, x2, y1, y2 = 0.97*xmid, 1.03*xmid, -0.10, 0.10 # specify the limits
				axins.set_xlim(x1, x2) # apply the x-limits
				axins.set_ylim(y1, y2) # apply the y-limits
				
				print axins.get_xticks()
				axins.set_xticks([2*Npa-1],(r"$M$"))
				print axins.get_xticks()
				axins.tick_params(direction='in', which='both', labelsize=0, top=False, left=False, right=False, bottom=False)
				plt.yticks(visible=False)
				plt.xticks(visible=False)
				# axins.grid()
			
			plt.savefig(r"GXMG_%s.png" %(labelName(U,V,mu,t,tdiag,tc)), dpi=200)
			plt.savefig(r"GXMG_%s.pdf" %(labelName(U,V,mu,t,tdiag,tc)))
			plt.close()
			# plt.show()
			
			
			
			
	if True and minPhase == 'b':
		ks1 = np.linspace(-np.pi, 0.0, nks)
		ks2 = np.linspace(0.0, np.pi, nks)
		boundary11 = -np.pi - ks1
		boundary12 = np.pi + ks1
		boundary21 = np.pi - ks2
		boundary22 = -np.pi + ks2
		
		K = np.linspace(-np.pi, np.pi, nks)
		mkx, mky = np.meshgrid(K, K, indexing='ij', copy=False)
		
		Fk_b1 = Free_energy_b1_k(minphia, t, tdiag, tc, mu, V, U, T, N=nks)
		
		fmax = np.max(np.abs(Fk_b1))
		
		fig = plt.figure(figsize=(10,7))
		ax = Axes3D(fig)
		# ax.plot_surface(mkx, mky, Fk_a1, rstride=1, cstride=1, cmap='seismic',vmin=-fmax,vmax=fmax,alpha=0.5)
		ax.plot_surface(mkx, mky, Fk_b1, rstride=1, cstride=1, cmap='PuOr_r',vmin=-fmax,vmax=fmax)
		# ax.plot_surface(mkx, mky, -Fk_a1, rstride=1, cstride=1, cmap='seismic',vmin=-fmax,vmax=fmax, alpha=0.5)
		ax.plot_surface(mkx, mky, -Fk_b1, rstride=1, cstride=1, cmap='PuOr_r',vmin=-fmax,vmax=fmax)
		ax.set_xlabel(r"$k_x$",fontsize=fs_label, labelpad=18)
		ax.set_ylabel(r"$k_y$",fontsize=fs_label, labelpad=18)
		ax.set_zlabel("Energy", fontsize=fs_label, labelpad=10)
		ax.view_init(elev=10,azim=35)
		
		ax.tick_params(direction='in', which='both', labelsize=fs_ticks, top=False, left=False, right=False, bottom=False)
		
		ax.xaxis.set_major_locator(plt.MaxNLocator(5))
		ax.yaxis.set_major_locator(plt.MaxNLocator(5))
		ax.zaxis.set_major_locator(plt.MaxNLocator(5))
		
		# z label rotation
		ax.zaxis.set_rotate_label(False)
		a = ax.zaxis.label.get_rotation()
		print a
		if a<180:
			a += 90#180
		ax.zaxis.label.set_rotation(a)

		plt.savefig(r"surfaceE_%s_phase_b.png" %(labelName(U,V,mu,t,tdiag,tc)), dpi=200)
		plt.savefig(r"surfaceE_%s_phase_b.pdf" %(labelName(U,V,mu,t,tdiag,tc)))
		plt.close()
		#plt.show()
		
		
		if True:
			### plot GammaXM Scheme
			Npa = nks
			Npa2 = np.int(np.sqrt(2)*Npa)
			GXMGpath_kx = []
			GXMGpath_ky = []
			### G -> X
			for kx in np.linspace(0.0, -np.pi/2,Npa):
				GXMGpath_kx.append(kx)
			for ky in np.linspace(0.0, np.pi/2,Npa):
				GXMGpath_ky.append(ky)
			### X -> M
			for kx in np.linspace(-np.pi/2, 0.0,Npa):
				GXMGpath_kx.append(kx)
			for ky in np.linspace(np.pi/2,np.pi,Npa):
				GXMGpath_ky.append(ky)
			### M -> G
			for kx in np.zeros(Npa2):
				GXMGpath_kx.append(kx)
			for ky in np.linspace(np.pi,0.0,Npa2):
				GXMGpath_ky.append(ky)
			
			GXMGpath_kx = np.array(GXMGpath_kx)
			GXMGpath_ky = np.array(GXMGpath_ky)
			
			
			pathE1 = Free_energy_b1_k_custom(minphia, GXMGpath_kx, GXMGpath_ky, t, tdiag, tc, mu, V, U, T, N=nks)
			# pathE2 = Free_energy_a2_k_custom(minphia, GXMGpath_kx, GXMGpath_ky, t, tdiag, tc, mu, V, U, T, N=Ngrid)
			
			
			plt.figure(figsize=(9,5))
			#plt.title(r"$\pm E_k$ ($U=%.3f$, $V=%.3f$, $\mu=%.3f$, $t=%.3f$, $t_{diag}=%.3f$, $t_c=%.3f$" %(U,V,mu,t,tdiag,tc))
			plt.plot(np.arange(len(pathE1)), pathE1, label=r'$E^1_k$',lw=lwg)
			plt.plot(np.arange(len(pathE1)), -pathE1, label=r'$E^3_k$',lw=lwg)
			# plt.plot(np.arange(len(pathE1)), pathE2, label=r'$E^2_k$',lw=lw)
			# plt.plot(np.arange(len(pathE1)), -pathE2, label=r'$E^4_k$',lw=lw)
			
			plt.axvline(0,ls='--',color='black',lw=lw)
			plt.axvline(Npa-1,ls='--',color='black',lw=lw)
			plt.axvline(2*Npa-1,ls='--',color='black',lw=lw)
			plt.axvline(2*Npa+Npa2-1,ls='--',color='black',lw=lw)
			plt.xticks([0,Npa-1,2*Npa-1,2*Npa+Npa2-1],(r"$\Gamma$",r"$X$",r"$M$",r"$\Gamma$"))
			plt.ylabel("Energy", fontsize=fs_label)
			plt.grid()
			
			ax = plt.gca()
			ax.tick_params(direction='in', which='both', labelsize=fs_ticks, top=False, left=False, right=False, bottom=False)

			ax.yaxis.set_major_locator(plt.MaxNLocator(5))
			
			plt.legend(fontsize=fs_legend, loc='center right')
			plt.tight_layout(pad=0.0)
			
			
			### inset
			if False:
				ax = plt.gca()
				axins = zoomed_inset_axes(ax, 4.0, loc='lower center')
				axins.plot(np.arange(len(pathE1)), pathE1, label=r'$E^1_k$',lw=lwg)
				axins.plot(np.arange(len(pathE1)), -pathE1, label=r'$E^3_k$',lw=lwg)
				
				mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
				
				x1, x2, y1, y2 = 1.9*Npa, 2.1*Npa, -0.20, 0.20 # specify the limits
				axins.set_xlim(x1, x2) # apply the x-limits
				axins.set_ylim(y1, y2) # apply the y-limits
				
				print axins.get_xticks()
				axins.set_xticks([2*Npa-1],(r"$M$"))
				print axins.get_xticks()
				axins.tick_params(direction='in', which='both', labelsize=0, top=False, left=False, right=False, bottom=False)
				plt.yticks(visible=False)
				plt.xticks(visible=False)
				# axins.grid()


			plt.savefig(r"GXMG_%s_phase_b.png" %(labelName(U,V,mu,t,tdiag,tc)), dpi=200)
			plt.savefig(r"GXMG_%s_phase_b.pdf" %(labelName(U,V,mu,t,tdiag,tc)))
			
			plt.close()	
			
			
    
if __name__ == "__main__":
	main()
