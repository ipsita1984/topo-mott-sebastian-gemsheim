#include <stdio.h>
#include <math.h>
#include <complex.h>
#include "integrand_function.h"

double integrandEa(int n, double *k, void *user_d) {
	/*  k[0] = kx   k[1] = ky  */
	double *d = (double *)user_d;      // parameter
	// d = phia,phib,t,tdiag,tc,mu,V,U,T
	//     0    1    2 3     4  5  6 7 8
	
	double ga = 2.0 * d[7];
	double ek = -2.0 * d[2] * ( cos(k[0]) + cos(k[1]) );
	double ekb = -2.0 * d[3] * sin(k[0]) * sin(k[1]);
	double E1 = sqrt( ek*ek + (ekb+d[0]*ga)*(ekb+d[0]*ga) );
	double E2 = sqrt( ek*ek + (ekb-d[0]*ga)*(ekb-d[0]*ga) );
	double E3 = -E1;
	double E4 = -E2;
	
	double E = 0.0;
	if (E1 <= d[5]) {
		E = E + E1;
	}
	if (E2 <= d[5]) {
		E = E + E2;
	}
	if (E3 <= d[5]) {
		E = E + E3;
	}
	if (E4 <= d[5]) {
		E = E + E4;
	}
	
	return E;
}

double integrandEb(int n, double *k, void *user_d) {
	/*  k[0] = kx   k[1] = ky  */
	double *d = (double *)user_d;      // parameter
	// d = phia,phib,t,tdiag,tc,mu,V,U,T
	//     0    1    2 3     4  5  6 7 8
	
	double gb = 8.0*d[6] + 24.0*d[4];
	double f = cos(k[0]) - cos(k[1]);
	double ek = -2.0 * d[2] * ( cos(k[0]) + cos(k[1]) );
	double ekb = -2.0 * d[3] * sin(k[0]) * sin(k[1]);
	double E2 = sqrt( ek*ek + ekb*ekb + (gb*d[1])*(gb*d[1]) * f*f );
	double E1 = -E2;
	
	double E = 0.0;
	if (E1 <= d[5]) {
		E = E + E1;
	}
	if (E2 <= d[5]) {
		E = E + E2;
	}
	
	return 2.0*E;
}

double integrandEab(int n, double *k, void *user_d) {
	/*  k[0] = kx   k[1] = ky  */
	double *d = (double *)user_d;      // parameter
	// d = phia,phib,t,tdiag,tc,mu,V,U,T
	//     0    1    2 3     4  5  6 7 8
	
	double ga = 2.0 * d[7];
	double gb = 8.0*d[6] + 24.0*d[4];
	double f = cos(k[0]) - cos(k[1]);
	double ek = -2.0 * d[2] * ( cos(k[0]) + cos(k[1]) );
	double ekb = -2.0 * d[3] * sin(k[0]) * sin(k[1]);
	double E1 = sqrt( ek*ek + (ekb+d[0]*ga)*(ekb+d[0]*ga) + (gb*d[1])*(gb*d[1]) * f*f );
	double E2 = sqrt( ek*ek + (ekb-d[0]*ga)*(ekb-d[0]*ga) + (gb*d[1])*(gb*d[1]) * f*f );
	double E3 = -E1;
	double E4 = -E2;
	
	double E = 0.0;
	if (E1 <= d[5]) {
		E = E + E1;
	}
	if (E2 <= d[5]) {
		E = E + E2;
	}
	if (E3 <= d[5]) {
		E = E + E3;
	}
	if (E4 <= d[5]) {
		E = E + E4;
	}
	
	return E;
}


int main(void) {
	return 0;
}
