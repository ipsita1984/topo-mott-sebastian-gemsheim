import numpy as np
import time
import os
import matplotlib.pyplot as plt
from Free_energy import *
from scipy import optimize
from scipy import ndimage
import matplotlib.patches as mpatches
import matplotlib
from scipy import interpolate


### TEST TO MAKE FINER GRID ------------------------------------------------------------

def regrid(data, out_x, out_y):
    m = max(data.shape[0], data.shape[1])
    y = np.linspace(0, 1.0/m, data.shape[0])
    x = np.linspace(0, 1.0/m, data.shape[1])
    interpolating_function = interpolate.RegularGridInterpolator((y, x), data)

    yv, xv = np.meshgrid(np.linspace(0, 1.0/m, out_y), np.linspace(0, 1.0/m, out_x))

    return interpolating_function((xv, yv))

### DEFINE FUNCTIONS TO FIND MINIMUM ------------------------------------------------------------

def readFile(filename):
	data = []     # matrix
	
	f = open(filename, "rU")
	p = f.readline().split("\n")[0].split("\t")	# read parameters
	for line in f:
		tmp = line.split("\t\n")[0]
		el = np.array(tmp.split("\t"))
		
		data.append( el.astype(np.float) )
	f.close()

	### read out parameters
	t = np.float(p[1])
	tdiag = np.float(p[3])
	tc = np.float(p[5])
	T = np.float(p[7])
	#variable = p[8]
	mu = np.float(p[9])
	Vmin = np.float(p[11])
	Vmax = np.float(p[13])
	NV = np.int(p[15])
	Umin = np.float(p[17])
	Umax = np.float(p[19])
	NU = np.int(p[21])

	return np.array(data), t, tdiag, tc, T, mu, Vmin, Vmax, NV, Umin, Umax, NU
	
	
def checkCDW(U, V, tc):
	if 8.0*V + 12*tc - U <= 0.0:
		return False
	else:
		return True

### MAIN FUNCTION ------------------------------------------------------------
		
def main(args):
	### read data
	foldername = args[0]
	
	SAVE = True
	
	dictData = dict()
	
	### get label
	if foldername[-1] == '/':
		foldername = foldername[:-1]
	print foldername
	folderinput = os.getcwd() + '/' + foldername + '/'
	print folderinput
	
	label = foldername.split('.')[0].split('minimization_fixed_mu')[-1]
	print label
	
	### read data
	items = ['fe','feA','feB','phi','phiA','phiB','phase']
	for item in items:
		dictData[item], t, tdiag, tc, T, mu, Vmin, Vmax, NV, Umin, Umax, NU = readFile(folderinput + item + label + '.txt')
	
	Us = np.linspace(Umin, Umax, NU)
	
	FE = dictData['fe']
	FEa = dictData['feA']
	FEb = dictData['feB']
	phi = dictData['phi']
	phia = dictData['phiA']
	phib = dictData['phiB']
	phase = dictData['phase']
	
	### CHECK FOR DATA POINTS FOR WHICH phi_a AND phi_b ARE ZERO!
	if True:
		for idxv in np.arange(phia.shape[0]):	# phia.shape[0] = Nmu
			for idxU in np.arange(phia.shape[1]):
				if np.isclose(phia[idxv,idxU], 0.0, atol=1e-4) and np.isclose(phib[idxv,idxU], 0.0, atol=1e-4):
					print 'yes!'
					phase[idxv, idxU] = np.nan
	
	# make phase nicer
	
	maskNaN = np.isnan(phase)
	
	if True:
		for idxv in np.arange(phia.shape[0]):	# phia.shape[0] = Nmu
			for idxU in np.arange(phia.shape[1]):
				if np.isnan(phase[idxv, idxU]):
					if Us[idxU] <= 1.17:
						phase[idxv, idxU] = 2
					else:
						phase[idxv, idxU] = 1
	
	# make FE nicer
	if True:
		for idxU in np.arange(phia.shape[1]):
			tmp = 0.0
			found = False
			
			if np.isnan(FE[0, idxU]):
				continue
			
			for idxv in np.arange(phia.shape[0]):
				if np.isnan(FE[idxv, idxU]) and not found:
					found = True
					tmp = FE[idxv-1, idxU]
					FE[idxv, idxU] = tmp
				elif found:
					FE[idxv, idxU] = tmp
	

	out_x = 3000
	out_y = out_x
	if True:
		rgphase = phase
		rgphase = ndimage.gaussian_filter(phase, sigma=2.3)
		rgphase = regrid(rgphase, out_x, out_y)
		rgphase = np.around(rgphase)
E

	else:
		rgphase = ndimage.gaussian_filter(phase, sigma=2.3)
		rgphase = regrid(rgphase, out_x, out_y)
		rgphase = np.around(rgphase)
	
	rgfe = regrid(FE, out_x, out_y)
	
	# make new Us and Vs
	print Umax, Umin, Vmax, Vmin
	Us = np.linspace(Umin, Umax, out_x)
	Vs = np.linspace(Vmin, Vmax, out_y)
	
	vcCDW = np.vectorize(checkCDW)
	
	mUs, mVs = np.meshgrid(Us, Vs, indexing='xy')
	
	maskNaN = vcCDW(mUs, mVs, tc)
	
	rgphase[maskNaN] = np.nan
	rgfe[maskNaN] = np.nan
	
	
	
	### Make plots
	
	phimax = np.max(np.abs(phi))
	phimaxa = np.max(np.abs(phia))
	phimaxb = np.max(np.abs(phib))
	
	
	interp = 'bilinear' 
	#interp = 'spline36' 
	interp = 'none'
	interp = 'nearest'
	
	### fontsizes 
	fs_ticks = 16
	fs_suptitle = 24
	fs_title = 24
	fs_label = 20
	fs_legend = 20
	
	if False:
		plt.figure(figsize=(16,9))
		#plt.suptitle(r"TEST $U=%.2f$, $t=%.2f$, $t_{diag}=%.2f$, $t_c=%.2f$, $T=%.2f$" %(U,t,tdiag,tc,T), fontsize=fs_suptitle, fontweight='bold', fontname='Arial')
		#plt.suptitle("Minimization", fontsize=fs_suptitle, fontweight='bold')
		plt.subplot(221)
		plt.title("Free energy A", fontsize=fs_title)
		plt.imshow(FEa, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto')
		cbar = plt.colorbar()
		cbar.ax.tick_params(labelsize=fs_ticks)
		plt.ylabel(r'$V$', fontsize=fs_label)
		plt.xlabel(r'$\mu$', fontsize=fs_label)
		ax = plt.gca()
		plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
		plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
		# To specify the number of ticks on both
		plt.locator_params(axis='y', nbins=6)
		plt.locator_params(axis='x', nbins=5)
		plt.subplot(222)
		plt.title(r"Order parameter $2\phi_a/g_a$", fontsize=fs_title)
		plt.imshow(phia, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=0.0, vmax=phimaxa)
		cbar = plt.colorbar()
		cbar.ax.tick_params(labelsize=fs_ticks)
		plt.ylabel(r'$V$', fontsize=fs_label)
		plt.xlabel(r'$\mu$', fontsize=fs_label)
		ax = plt.gca()
		plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
		plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
		# To specify the number of ticks on both
		plt.locator_params(axis='y', nbins=6)
		plt.locator_params(axis='x', nbins=5)
		plt.subplot(223)
		plt.title("Free energy B", fontsize=fs_title)
		plt.imshow(FEb, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto')
		cbar = plt.colorbar()
		cbar.ax.tick_params(labelsize=fs_ticks)
		plt.ylabel(r'$V$', fontsize=fs_label)
		plt.xlabel(r'$\mu$', fontsize=fs_label)
		ax = plt.gca()
		plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
		plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
		# To specify the number of ticks on both
		plt.locator_params(axis='y', nbins=6)
		plt.locator_params(axis='x', nbins=5)
		plt.subplot(224)
		plt.title(r"Order parameter $2\phi_b/g_b$", fontsize=fs_title)
		plt.imshow(phib, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=0.0, vmax=phimaxb)
		cbar = plt.colorbar()
		cbar.ax.tick_params(labelsize=fs_ticks)
		plt.ylabel(r'$V$', fontsize=fs_label)
		plt.xlabel(r'$\mu$', fontsize=fs_label)
		ax = plt.gca()
		plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
		plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
		# To specify the number of ticks on both
		plt.locator_params(axis='y', nbins=6)
		plt.locator_params(axis='x', nbins=5)
		plt.tight_layout(rect=[0, 0, 1, 1], pad=1.0, w_pad=1.0, h_pad=1.0)
		if SAVE:
			plt.savefig(folderinput + "individual_minimization_fix_mu" + label + ".png", dpi=150)
			plt.savefig(folderinput + "individual_minimization_fix_mu" + label + ".pdf", dpi=150)
		#plt.show()
		
		plt.figure(figsize=(20,7))
		#plt.figure()
		#plt.suptitle(r"$U=%.2f$, $t=%.2f$, $t_{diag}=%.2f$, $t_c=%.2f$, $T=%.2f$" %(U,t,tdiag,tc,T), fontsize=fs_suptitle, fontweight='bold')
		#plt.suptitle("Minimization", fontsize=fs_suptitle, fontweight='bold')
		plt.subplot(131)
		plt.title("Free energy", fontsize=fs_title)
		plt.imshow(FE, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto')
		cbar = plt.colorbar()
		cbar.ax.tick_params(labelsize=fs_ticks)
		plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
		plt.xlabel(r'$\mu$', fontsize=fs_label, fontweight='bold')
		ax = plt.gca()
		plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
		plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
		# To specify the number of ticks on both
		plt.locator_params(axis='y', nbins=6)
		plt.locator_params(axis='x', nbins=5)
		plt.subplot(132)
		plt.title("Phase", fontsize=fs_title)
		plt.imshow(phase, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=1.0, vmax=2.0)
		#plt.colorbar()
		cmap = matplotlib.cm.get_cmap('viridis')
		sdwpatch = mpatches.Patch(color=cmap(0.0), alpha=0.8, label='SDW')
		ddwpatch = mpatches.Patch(color=cmap(1.0), alpha=0.8, label='DDW')
		plt.legend(loc='upper right', handles=[sdwpatch,ddwpatch], fontsize=fs_legend)
		#plt.grid()
		plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
		plt.xlabel(r'$\mu$', fontsize=fs_label, fontweight='bold')
		ax = plt.gca()
		plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
		plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
		# To specify the number of ticks on both
		plt.locator_params(axis='y', nbins=6)
		plt.locator_params(axis='x', nbins=5)
		plt.subplot(133)
		plt.title(r"Order parameter $2\phi/g$", fontsize=fs_title)
		plt.imshow(phi, cmap='seismic', interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=-phimax, vmax=phimax)
		cbar = plt.colorbar()
		cbar.ax.tick_params(labelsize=fs_ticks)
		plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
		plt.xlabel(r'$\mu$', fontsize=fs_label, fontweight='bold')
		ax = plt.gca()
		plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
		plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
		redpatch = mpatches.Patch(color='red', alpha=0.8, label='SDW')
		bluepatch = mpatches.Patch(color='blue', alpha=0.8, label='DDW')
		plt.legend(loc='upper right', handles=[redpatch,bluepatch], fontsize=fs_legend)
		# To specify the number of ticks on both
		plt.locator_params(axis='y', nbins=6)
		plt.locator_params(axis='x', nbins=5)
		plt.tight_layout(rect=[0, 0, 1, 1], pad=1.0, w_pad=4.0, h_pad=4.0)
		if SAVE:
			plt.savefig(folderinput + "phase_diagram_fix_mu" + label + ".png", dpi=150)
			plt.savefig(folderinput + "phase_diagram_fix_mu" + label + ".pdf", dpi=150)
		#plt.show()
	
	
	
	### smoothening
	
	fs_label = 22#18
	fs_ticks = 20#16
	fs_legend = fs_label
	
	lw = 2
	
	plt.figure(figsize=(11,5))
	#plt.figure()
	#plt.suptitle(r"$U=%.2f$, $t=%.2f$, $t_{diag}=%.2f$, $t_c=%.2f$, $T=%.2f$" %(U,t,tdiag,tc,T), fontsize=fs_suptitle, fontweight='bold')
	#plt.suptitle("Minimization", fontsize=fs_suptitle, fontweight='bold')
	plt.subplot(121)
	plt.title("Free energy", fontsize=fs_title)
	plt.imshow(rgfe, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto')
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$U$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	
	# To specify the number of ticks on both
	# plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	ax.yaxis.set_major_locator(plt.MaxNLocator(5))
	
	plt.subplot(122)
	plt.title("Phase", fontsize=fs_title)
	plt.imshow(rgphase, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto', vmin=1.0, vmax=2.0)
	
	#plt.colorbar()
	cmap = matplotlib.cm.get_cmap('viridis')
	sdwpatch = mpatches.Patch(color=cmap(0.0), alpha=0.8, label='SDW')
	ddwpatch = mpatches.Patch(color=cmap(1.0), alpha=0.8, label='DDW')
	plt.legend(loc='upper left', handles=[sdwpatch,ddwpatch], fontsize=fs_legend)
	
	#plt.grid()
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$U$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	
	# To specify the number of ticks on both
	# plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	ax.yaxis.set_major_locator(plt.MaxNLocator(5))
	
	
	plt.tight_layout(pad=0, w_pad=1.0)
	# plt.tight_layout(rect=[0, 0, 1, 1], pad=1.0, w_pad=4.0, h_pad=4.0)
	
	if SAVE:
		plt.savefig(folderinput + "phase_diagram_smooth_fix_mu" + label + ".png", dpi=200)
		plt.savefig(folderinput + "phase_diagram_smooth_fix_mu" + label + ".pdf")
	plt.show()
	
    
if __name__ == "__main__":
	main(sys.argv[1:])
