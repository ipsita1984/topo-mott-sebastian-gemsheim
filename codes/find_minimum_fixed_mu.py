import numpy as np
import time
import os
import matplotlib.pyplot as plt
import sys
from Free_energy2 import *
from scipy import optimize
import matplotlib.patches as mpatches
import matplotlib

tol = 1e-7
Ngrid = 80

### DEFINE FUNCTIONS TO READ OUT PARAMETERS -----------------------------------------------------

def readParams(filename):
	### read file
	f = open(filename, 'rU')
	params = dict()
	for line in f:
		elements = line.split('\n')[0].split(';')
		params[elements[0]] = elements[1]
	f.close()
	### assign parameters
	t = np.float(params['t'])
	tc = np.float(params['tc'])
	tdiag = np.float(params['tdiag'])
	T = np.float(params['T'])
	mu = np.float(params['mu'])
	Umin = np.float(params['Umin'])
	Umax = np.float(params['Umax'])
	NV = np.int(params['NV'])
	NU = np.int(params['NU'])
	label = params['label']
	
	
	return t,tc,tdiag,T,mu,NV,Umin,Umax,NU, label

### CHECK CDW RANGE -----------------------------------------------------

def checkCDW(U, V, tc):
	if 8.0*V + 12*tc - U <= 0.0:
		return True
	else:
		return False
	
def getMaxV(U, tc):
    return U / 8.0 - 1.5 * tc

### DEFINE FUNCTIONS TO FIND MINIMUM ------------------------------------------------------------

def getMinEnergyA(t, tdiag, tc, mu, V, U, T):
	### find initial guess
	minbnd = 0.0
	maxbnd = 2.0
	igs = np.linspace(minbnd,maxbnd,Ngrid)       # emperical way to find GLOBAL minimum
	fes = Free_energy_C_a2(igs, t, tdiag, tc, mu, V, U, T)
	x0 = igs[np.argmin(fes)]
	fe0 = np.min(fes)
	return x0, fe0

def getMinEnergyB(t, tdiag, tc, mu, V, U, T):
	### find initial guess
	minbnd = 0.0
	maxbnd = 2.0
	igs = np.linspace(minbnd,maxbnd,Ngrid)       # emperical way to find GLOBAL minimum
	fes = Free_energy_C_b2(igs, t, tdiag, tc, mu, V, U, T)
	x0 = igs[np.argmin(fes)]
	fe0 = np.min(fes)
	return x0, fe0

### MAIN FUNCTION ------------------------------------------------------------
		
def main():
	### parameters
	fileParam = 'params_fixed_mu.txt'
	try:
		t,tc,tdiag,T,mu,NV,Umin,Umax,NU,additional_label = readParams(fileParam)
	except:
		print "Couldn't read parameter file! Use in-code parameters!"
		t = 1.0
		tdiag = 0.9
		tc = 0.09
		T = 0.0
		mu = 0.1
		
		NV = 101
		
		Umin = 0.0
		Umax = 4.0
		NU = NV
		
	### calculate V range for which CDW phase can be neglected
	Vmin = getMaxV(Umin, tc)
	if Vmin < 0.0:
		Vmin = 0.0
	Vmax = getMaxV(Umax, tc)
	
	print ""
	print "t = %.2f" %(t)
	print "tdiag = %.2f" %(tdiag)
	print "tc = %.2f" %(tc)
	print "mu = %.2f" %(mu)
	print "Vmin = %.2f" %(Vmin)
	print "Vmax = %.2f" %(Vmax)
	print "NV = %i" %(NV)
	print "Umin = %.2f" %(Umin)
	print "Umax = %.2f" %(Umax)
	print "NU = %i" %(NU)
	print "T = %.2f" %(T)
	print "label = " + additional_label
	print ""
	
	
	### output yes or no
	SAVE = True
	
	### Initialize arrays
	Vs = np.linspace(Vmin, Vmax, NV)
	Us = np.linspace(Umin, Umax, NU);
	
	FE = np.zeros((NV,NU))
	FEa = np.zeros((NV,NU))
	FEb = np.zeros((NV,NU))
	phase = np.zeros((NV,NU))
	phi = np.zeros((NV,NU))
	phia = np.zeros((NV,NU))
	phib = np.zeros((NV,NU))
	
	
	### Prepare output files

	# use parameters and day as file name
	date = time.localtime()
	name = "%i-%i-%i_%i-%i-%i" %(date[0],date[1],date[2],date[3],date[4],date[5]) + additional_label
	
	if SAVE:
		# make output folder
		folderOutput = os.getcwd() + "/minimization_fixed_mu_" + name + "/"
		if not os.path.isdir(folderOutput):
				os.mkdir(folderOutput)
		
		namePhiA = folderOutput + "phiA_" + name  + ".txt"
		namePhiB = folderOutput + "phiB_" + name  + ".txt"
		nameFeA = folderOutput + "feA_" + name  + ".txt"
		nameFeB = folderOutput + "feB_" + name  + ".txt"
		namePhi = folderOutput + "phi_" + name  + ".txt"
		namePhase = folderOutput + "phase_" + name  + ".txt"
		nameFe = folderOutput + "fe_" + name  + ".txt"
		
		for iname in [namePhiA,namePhiB,nameFeA,nameFeB,nameFe,namePhi,namePhase]:
			f = open(iname, "w")
			### write header with parameters in all output files
			f.write("t\t%.5f\ttdiag\t%.5f\ttc\t%.5f\tT\t%.5f\tmu\t%.5f\tVmin\t%.5f\tVmax\t%.5f\tNV\t%i\tUmin\t%.5f\tUmax\t%.5f\tNU\t%i\n" %(t,tdiag,tc,T,mu,Vmin,Vmax,NV,Umin,Umax,NU))
			f.close()
	
	starttime = time.time()
	print "Start calculations!"
	for idxv, V in enumerate(Vs):
		print idxv+1, ' / ', NV
		for idxU, U in enumerate(Us):
			if checkCDW(U,V,tc):
				phiamin, Eamin = getMinEnergyA(t, tdiag, tc, mu, V, U, T)
				phibmin, Ebmin = getMinEnergyB(t, tdiag, tc, mu, V, U, T)
				phia[idxv,idxU] = phiamin
				phib[idxv,idxU] = phibmin
				FEa[idxv,idxU] = Eamin
				FEb[idxv,idxU] = Ebmin
				FE[idxv,idxU] = np.min([Eamin, Ebmin])
				phase[idxv,idxU] = np.argmin([Eamin, Ebmin]) + 1 
				if Eamin < Ebmin:
					phi[idxv, idxU] = phiamin
				else:
					phi[idxv, idxU] = -phibmin
			else:
				phia[idxv,idxU] = np.nan
				phib[idxv,idxU] = np.nan
				FEa[idxv,idxU] = np.nan
				FEb[idxv,idxU] = np.nan
				FE[idxv,idxU] = np.nan
				phase[idxv,idxU] = np.nan
				phi[idxv, idxU] = np.nan
		if SAVE:		
			### output
			f = open(namePhiA, "a")
			for idxU in np.arange(NU):
				f.write("%.6f\t"  %(phia[idxv,idxU]))
			f.write("\n")
			f.close()
			
			f = open(namePhiB, "a")
			for idxU in np.arange(NU):
				f.write("%.6f\t"  %(phib[idxv,idxU]))
			f.write("\n")
			f.close()
			
			f = open(nameFeA, "a")
			for idxU in np.arange(NU):
				f.write("%.6f\t"  %(FEa[idxv,idxU]))
			f.write("\n")
			f.close()
			
			f = open(nameFeB, "a")
			for idxU in np.arange(NU):
				f.write("%.6f\t"  %(FEb[idxv,idxU]))
			f.write("\n")
			f.close()
			
			f = open(namePhi, "a")
			for idxU in np.arange(NU):
				f.write("%.6f\t"  %(phi[idxv,idxU]))
			f.write("\n")
			f.close()
			
			f = open(nameFe, "a")
			for idxU in np.arange(NU):
				f.write("%.6f\t"  %(FE[idxv,idxU]))
			f.write("\n")
			f.close()
			
			f = open(namePhase, "a")
			for idxU in np.arange(NU):
				f.write("%.6f\t"  %(phase[idxv,idxU]))
			f.write("\n")
			f.close()
		
	totaltime = time.time()-starttime
	print "Done! Execution time: %.1f s / %.1f min / %.1f h" %(totaltime,totaltime/60.0,totaltime/3600.0)
	print "time(step): %.3f s" %(totaltime/(NV*NU))
	
		
	
	if False:
		sys.exit()		# if the program should terminate here, no plots
	
	### Make plots
	maskphi = np.logical_not(np.isnan(phi))
	maskphia = np.logical_not(np.isnan(phia))
	maskphib = np.logical_not(np.isnan(phib))
	phimax = np.max(np.abs(phi[maskphi]))
	phimaxa = np.max(np.abs(phia[maskphia]))
	phimaxb = np.max(np.abs(phib[maskphib]))
	
	
	interp = 'bilinear' 
	interp = 'spline36' 
	interp = 'none' 
		
	### fontsizes 
	fs_ticks = 16
	fs_suptitle = 24
	fs_title = 24
	fs_label = 20
	fs_legend = 20
	
	plt.figure(figsize=(16,9))
	#plt.suptitle(r"TEST $U=%.2f$, $t=%.2f$, $t_{diag}=%.2f$, $t_c=%.2f$, $T=%.2f$" %(U,t,tdiag,tc,T), fontsize=fs_suptitle, fontweight='bold', fontname='Arial')
	#plt.suptitle("Minimization", fontsize=fs_suptitle, fontweight='bold')
	plt.subplot(221)
	plt.title("Free energy A", fontsize=fs_title)
	plt.imshow(FEa, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto')
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$U$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(222)
	plt.title(r"Order parameter $2\phi_a/g_a$", fontsize=fs_title)
	plt.imshow(phia, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto', vmin=0.0, vmax=phimaxa)
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$U$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(223)
	plt.title("Free energy B", fontsize=fs_title)
	plt.imshow(FEb, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto')
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$U$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(224)
	plt.title(r"Order parameter $2\phi_b/g_b$", fontsize=fs_title)
	plt.imshow(phib, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto', vmin=0.0, vmax=phimaxb)
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$U$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.tight_layout(rect=[0, 0, 1, 1], pad=1.0, w_pad=1.0, h_pad=1.0)
	if SAVE:
		plt.savefig(folderOutput + "individual_minimization_fix_mu_" + name + ".png", dpi=150)
		plt.savefig(folderOutput + "individual_minimization_fix_mu_" + name + ".pdf", dpi=150)
    
    
	plt.figure(figsize=(20,7))
	#plt.figure()
	#plt.suptitle(r"$U=%.2f$, $t=%.2f$, $t_{diag}=%.2f$, $t_c=%.2f$, $T=%.2f$" %(U,t,tdiag,tc,T), fontsize=fs_suptitle, fontweight='bold')
	#plt.suptitle("Minimization", fontsize=fs_suptitle, fontweight='bold')
	plt.subplot(131)
	plt.title("Free energy", fontsize=fs_title)
	plt.imshow(FE, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto')
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$U$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(132)
	plt.title("Phase", fontsize=fs_title)
	plt.imshow(phase, interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto', vmin=1.0, vmax=2.0)
	#plt.colorbar()
	cmap = matplotlib.cm.get_cmap('viridis')
	sdwpatch = mpatches.Patch(color=cmap(0.0), alpha=0.8, label='SDW')
	ddwpatch = mpatches.Patch(color=cmap(1.0), alpha=0.8, label='DDW')
	plt.legend(loc='upper right', handles=[sdwpatch,ddwpatch], fontsize=fs_legend)
	#plt.grid()
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$U$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(133)
	plt.title(r"Order parameter $2\phi/g$", fontsize=fs_title)
	plt.imshow(phi, cmap='seismic', interpolation=interp, origin='lower', extent=[Umin,Umax,Vmin,Vmax],aspect='auto', vmin=-phimax, vmax=phimax)
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$U$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	redpatch = mpatches.Patch(color='red', alpha=0.8, label='SDW')
	bluepatch = mpatches.Patch(color='blue', alpha=0.8, label='DDW')
	plt.legend(loc='upper right', handles=[redpatch,bluepatch], fontsize=fs_legend)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.tight_layout(rect=[0, 0, 1, 1], pad=1.0, w_pad=4.0, h_pad=4.0)
	if SAVE:
		plt.savefig(folderOutput + "phase_diagram_fix_mu_" + name + ".png", dpi=150)
		plt.savefig(folderOutput + "phase_diagram_fix_mu_" + name + ".pdf", dpi=150)
	plt.show()
    
if __name__ == "__main__":
	main()
