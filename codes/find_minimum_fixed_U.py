import numpy as np
import time
import os
import sys
import matplotlib.pyplot as plt
from Free_energy2 import *
from scipy import optimize
import matplotlib.patches as mpatches
import matplotlib

tol = 1e-7
Ngrid = 80

### DEFINE FUNCTIONS TO READ OUT PARAMETERS -----------------------------------------------------

def readParams(filename):
	### read file
	f = open(filename, 'rU')
	params = dict()
	for line in f:
		elements = line.split('\n')[0].split(';')
		params[elements[0]] = elements[1]
	f.close()
	### assign parameters
	t = np.float(params['t'])
	tc = np.float(params['tc'])
	tdiag = np.float(params['tdiag'])
	T = np.float(params['T'])
	U = np.float(params['U'])
	#Vmin = np.float(params['Vmin'])
	#Vmax = np.float(params['Vmax'])
	mumin = np.float(params['mumin'])
	mumax = np.float(params['mumax'])
	NV = np.int(params['NV'])
	Nmu = np.int(params['Nmu'])
	label = params['label']
	
	
	return t,tc,tdiag,T,U,NV,mumin,mumax,Nmu, label

### GET V RANGE -----------------------------------------------------

def getMaxV(U, tc):
    Vmax = U / 8.0 - 1.5 * tc
    if Vmax < 0.0:
        return 0.0
    else:
        return Vmax

### DEFINE FUNCTIONS TO FIND MINIMUM ------------------------------------------------------------
    
def getMinEnergyA(t, tdiag, tc, mu, V, U, T):
	minbnd = 0.0
	maxbnd = 2.0
	igs = np.linspace(minbnd,maxbnd,Ngrid)       # emperical way to find GLOBAL minimum
	fes = Free_energy_C_a2(igs, t, tdiag, tc, mu, V, U, T)
	x0 = igs[np.argmin(fes)]
	fe0 = np.min(fes)
	return x0, fe0
	
def getMinEnergyB(t, tdiag, tc, mu, V, U, T):
	minbnd = 0.0
	maxbnd = 2.0
	igs = np.linspace(minbnd,maxbnd,Ngrid)       # emperical way to find GLOBAL minimum
	fes = Free_energy_C_b2(igs, t, tdiag, tc, mu, V, U, T)
	x0 = igs[np.argmin(fes)]
	fe0 = np.min(fes)
	return x0, fe0

### MAIN FUNCTION ------------------------------------------------------------
		
def main():
	### parameters
	fileParam = 'params_fixed_U.txt'
	try:
		t,tc,tdiag,T,U,NV,mumin,mumax,Nmu,additional_label = readParams(fileParam)
	except:
		print "Couldn't read parameter file! Use in-code parameters!"
		t = 1.0
		tdiag = 0.75
		tc = 0.02
		T = 0.0
		U = 0.0
		Vmin = 0.0
		Vmax = 0.4
		NV = 101
		mumin = 0.0
		mumax = 4.0
		Nmu = NV
	
	
	### calculate V range for which CDW phase can be neglected
	Vmin = 0.0
	Vmax = getMaxV(U, tc)
	
	if np.isclose(Vmax, 0.0):
		print "No parameter range possible for neglected CDW phase"
		sys.exit()
	
	print ""
	print "t = %.2f" %(t)
	print "tdiag = %.2f" %(tdiag)
	print "tc = %.2f" %(tc)
	print "U = %.2f" %(U)
	print "Vmin = %.2f" %(Vmin)
	print "Vmax = %.2f" %(Vmax)
	print "mumin = %.2f" %(mumin)
	print "mumax = %.2f" %(mumax)
	print "T = %.2f" %(T)
	print ""
	
	
	### output yes or no
	SAVE = True
	
	### Initialize arrays
	Vs = np.linspace(Vmin, Vmax, NV)
	mus = np.linspace(mumin, mumax, Nmu);
	
	FE = np.zeros((NV,Nmu))
	FEa = np.zeros((NV,Nmu))
	FEb = np.zeros((NV,Nmu))
	phase = np.zeros((NV,Nmu))
	phi = np.zeros((NV,Nmu))
	phia = np.zeros((NV,Nmu))
	phib = np.zeros((NV,Nmu))
	
	
	### Prepare output files

	# use parameters and day as file name
	date = time.localtime()
	name = "%i-%i-%i_%i-%i-%i" %(date[0],date[1],date[2],date[3],date[4],date[5]) + additional_label
	
	
	# make output folder
	folderOutput = os.getcwd() + "/minimization_fixed_U_" + name + "/"
	if not os.path.isdir(folderOutput):
			os.mkdir(folderOutput)
	
	namePhiA = folderOutput + "phiA_" + name  + ".txt"
	namePhiB = folderOutput + "phiB_" + name  + ".txt"
	nameFeA = folderOutput + "feA_" + name  + ".txt"
	nameFeB = folderOutput + "feB_" + name  + ".txt"
	namePhi = folderOutput + "phi_" + name  + ".txt"
	namePhase = folderOutput + "phase_" + name  + ".txt"
	nameFe = folderOutput + "fe_" + name  + ".txt"
	
	for iname in [namePhiA,namePhiB,nameFeA,nameFeB,nameFe,namePhi,namePhase]:
		f = open(iname, "w")
		### write header with parameters in all output files
		f.write("t\t%.5f\ttdiag\t%.5f\ttc\t%.5f\tT\t%.5f\tU\t%.5f\tVmin\t%.5f\tVmax\t%.5f\tNV\t%i\tmumin\t%.5f\tmumax\t%.5f\tNmu\t%i\n" %(t,tdiag,tc,T,U,Vmin,Vmax,NV,mumin,mumax,Nmu))
		f.close()
	
	starttime = time.time()
	print "Start calculations!"
	for idxv, V in enumerate(Vs):
		print idxv+1, ' / ', NV
		for idxmu, mu in enumerate(mus):
			phiamin, Eamin = getMinEnergyA(t, tdiag, tc, mu, V, U, T)
			phibmin, Ebmin = getMinEnergyB(t, tdiag, tc, mu, V, U, T)
			phia[idxv,idxmu] = phiamin
			phib[idxv,idxmu] = phibmin
			FEa[idxv,idxmu] = Eamin
			FEb[idxv,idxmu] = Ebmin
			FE[idxv,idxmu] = np.min([Eamin, Ebmin])
			phase[idxv,idxmu] = np.argmin([Eamin, Ebmin]) + 1 
			if Eamin < Ebmin:
				phi[idxv, idxmu] = phiamin
			else:
				phi[idxv, idxmu] = -phibmin
			if np.isclose(phiamin, 0.0) and np.isclose(phibmin, 0.0):
				phase[idxv, idxmu] = np.nan
				
		### output
		f = open(namePhiA, "a")
		for idxmu in np.arange(Nmu):
			f.write("%.6f\t"  %(phia[idxv,idxmu]))
		f.write("\n")
		f.close()
		
		f = open(namePhiB, "a")
		for idxmu in np.arange(Nmu):
			f.write("%.6f\t"  %(phib[idxv,idxmu]))
		f.write("\n")
		f.close()
		
		f = open(nameFeA, "a")
		for idxmu in np.arange(Nmu):
			f.write("%.6f\t"  %(FEa[idxv,idxmu]))
		f.write("\n")
		f.close()
		
		f = open(nameFeB, "a")
		for idxmu in np.arange(Nmu):
			f.write("%.6f\t"  %(FEb[idxv,idxmu]))
		f.write("\n")
		f.close()
		
		f = open(namePhi, "a")
		for idxmu in np.arange(Nmu):
			f.write("%.6f\t"  %(phi[idxv,idxmu]))
		f.write("\n")
		f.close()
		
		f = open(nameFe, "a")
		for idxmu in np.arange(Nmu):
			f.write("%.6f\t"  %(FE[idxv,idxmu]))
		f.write("\n")
		f.close()
		
		f = open(namePhase, "a")
		for idxmu in np.arange(Nmu):
			f.write("%.6f\t"  %(phase[idxv,idxmu]))
		f.write("\n")
		f.close()
		
	totaltime = time.time()-starttime
	print "Done! Execution time: %.1f s / %.1f min / %.1f h" %(totaltime,totaltime/60.0,totaltime/3600.0)
	print "time(step): %.3f s" %(totaltime/(NV*Nmu))
	
	
	
	if False:
		sys.exit()		# if the program should terminate here, no plots
	
	### Make plots
	    
	phimax = np.max(np.abs(phi))
	phimaxa = np.max(np.abs(phia))
	phimaxb = np.max(np.abs(phib))
	
	### CHECK FOR DATA POINTS FOR WHICH phi_a AND phi_b ARE ZERO!
	if True:
		for idxv in np.arange(phia.shape[0]):	
			for idxmu in np.arange(phia.shape[1]):
				if np.isclose(phia[idxv,idxmu], 0.0, atol=1e-4) and np.isclose(phib[idxv,idxmu], 0.0, atol=1e-4):
					phase[idxv, idxmu] = np.nan
	
	
	interp = 'bilinear' 
	interp = 'spline36' 
	interp = 'none' 
		
	### fontsizes 
	fs_ticks = 16
	fs_suptitle = 24
	fs_title = 24
	fs_label = 20
	fs_legend = 20
	
	plt.figure(figsize=(16,9))
	#plt.suptitle(r"TEST $U=%.2f$, $t=%.2f$, $t_{diag}=%.2f$, $t_c=%.2f$, $T=%.2f$" %(U,t,tdiag,tc,T), fontsize=fs_suptitle, fontweight='bold', fontname='Arial')
	#plt.suptitle("Minimization", fontsize=fs_suptitle, fontweight='bold')
	plt.subplot(221)
	plt.title("Free energy A", fontsize=fs_title)
	plt.imshow(FEa, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto')
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$\mu$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(222)
	plt.title(r"Order parameter $2\phi_a/g_a$", fontsize=fs_title)
	plt.imshow(phia, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=0.0, vmax=phimaxa)
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$\mu$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(223)
	plt.title("Free energy B", fontsize=fs_title)
	plt.imshow(FEb, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto')
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$\mu$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(224)
	plt.title(r"Order parameter $2\phi_b/g_b$", fontsize=fs_title)
	plt.imshow(phib, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=0.0, vmax=phimaxb)
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label)
	plt.xlabel(r'$\mu$', fontsize=fs_label)
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.tight_layout(rect=[0, 0, 1, 1], pad=1.0, w_pad=1.0, h_pad=1.0)
	if SAVE:
		plt.savefig(folderOutput + "individual_minimization_fix_U_" + name + ".png", dpi=150)
		plt.savefig(folderOutput + "individual_minimization_fix_U_" + name + ".pdf", dpi=150)
    
    
	plt.figure(figsize=(20,7))
	#plt.figure()
	#plt.suptitle(r"$U=%.2f$, $t=%.2f$, $t_{diag}=%.2f$, $t_c=%.2f$, $T=%.2f$" %(U,t,tdiag,tc,T), fontsize=fs_suptitle, fontweight='bold')
	#plt.suptitle("Minimization", fontsize=fs_suptitle, fontweight='bold')
	plt.subplot(131)
	plt.title("Free energy", fontsize=fs_title)
	plt.imshow(FE, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto')
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$\mu$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(132)
	plt.title("Phase", fontsize=fs_title)
	plt.imshow(phase, interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=1.0, vmax=2.0)
	#plt.colorbar()
	cmap = matplotlib.cm.get_cmap('viridis')
	sdwpatch = mpatches.Patch(color=cmap(0.0), alpha=0.8, label='SDW')
	ddwpatch = mpatches.Patch(color=cmap(1.0), alpha=0.8, label='DDW')
	plt.legend(loc='upper right', handles=[sdwpatch,ddwpatch], fontsize=fs_legend)
	#plt.grid()
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$\mu$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.subplot(133)
	plt.title(r"Order parameter $2\phi/g$", fontsize=fs_title)
	plt.imshow(phi, cmap='seismic', interpolation=interp, origin='lower', extent=[mumin,mumax,Vmin,Vmax],aspect='auto', vmin=-phimax, vmax=phimax)
	cbar = plt.colorbar()
	cbar.ax.tick_params(labelsize=fs_ticks)
	plt.ylabel(r'$V$', fontsize=fs_label, fontweight='bold')
	plt.xlabel(r'$\mu$', fontsize=fs_label, fontweight='bold')
	ax = plt.gca()
	plt.setp(ax.get_xticklabels(), fontsize=fs_ticks)
	plt.setp(ax.get_yticklabels(), fontsize=fs_ticks)
	redpatch = mpatches.Patch(color='red', alpha=0.8, label='SDW')
	bluepatch = mpatches.Patch(color='blue', alpha=0.8, label='DDW')
	plt.legend(loc='upper right', handles=[redpatch,bluepatch], fontsize=fs_legend)
	# To specify the number of ticks on both
	plt.locator_params(axis='y', nbins=6)
	plt.locator_params(axis='x', nbins=5)
	plt.tight_layout(rect=[0, 0, 1, 1], pad=1.0, w_pad=4.0, h_pad=4.0)
	if SAVE:
		plt.savefig(folderOutput + "phase_diagram_fix_U_" + name + ".png", dpi=150)
		plt.savefig(folderOutput + "phase_diagram_fix_U_" + name + ".pdf", dpi=150)
	plt.show()
    
if __name__ == "__main__":
	main()
