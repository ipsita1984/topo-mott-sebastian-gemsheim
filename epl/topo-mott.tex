\documentclass[aps,prb,preprint,twocolumn,citeautoscript,eqsecnum,10pt,showpacs]{revtex4-1}  
\synctex=1 

\pdfoutput=1

\usepackage{amsmath,amssymb,bm} 
\usepackage{graphicx}

\usepackage[tight]{subfigure} 

\usepackage{color} 
\usepackage[papersize={8.5in,11in}]{geometry}
\usepackage[colorlinks=true]{hyperref}
\hypersetup{
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters 
    pdftoolbar=true,        % show Acrobat
    pdfmenubar=true,        % show Acrobat 
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={My title},    % title
    pdfauthor={Author},     % author
    pdfsubject={Subject},   % subject of the document
    pdfcreator={Creator},   % creator of the document
    pdfproducer={Producer}, % producer of the document
    pdfkeywords={keyword1} {key2} {key3}, % list of keywords
    pdfnewwindow=true,      % links in new window
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=magenta, %red,          % color of internal links (change box color with linkbordercolor)
    citecolor=blue,        % color of links to bibliography
    filecolor=magenta,      % color of file links
    urlcolor=blue           % color of external links
} 

\geometry{top=2.5cm, left=2cm, right=2cm, bottom=2.5cm}        

\linespread{1.25}
%\usepackage{showkeys}
\usepackage{amsfonts,bbold}
\usepackage{upgreek}
\usepackage{slashed}
\usepackage{latexsym}
\usepackage{ulem}

\usepackage{wrapfig} % to include figure with text wrapping around it
\usepackage{scrpage2}						
\usepackage{wasysym}			
\usepackage{amsthm}
\usepackage{xcolor}






\newcommand{\nn}{\nonumber \\}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}


\def\bx{\boldsymbol{x}}
\renewcommand{\vec}[1]{{\bf #1}}


\newcommand{\diag}{\mathrm{diag}}
\newcommand\dvec{{\boldsymbol{d}}}
\newcommand{\Tr}{{\rm Tr}\,}
\newcommand{\vare}{\varepsilon}
\newcommand{\sgn}{\mbox{sgn}}
\newcommand{\rmi}{{\rm i\,}}
\newcommand{\mathJ}{\mathcal{J}}


\newcommand{\addRN}[1]{\textcolor{red}{#1}}

\renewcommand{\epsilon}{\varepsilon}





\def\be{\begin{eqnarray}}
\def\ee{\end{eqnarray}}



\bibliographystyle{apsrev4-1}

\begin{document}


\title{Emergence of topological Mott insulators in proximity of quadratic band touching points}


\author{Ipsita Mandal}
\affiliation{Laboratory of Atomic And Solid State Physics, Cornell University, Ithaca, NY 14853, USA}

\author{Sebastian Gemsheim}
\affiliation{Max-Planck Institute for the Physics of Complex Systems, Noethnitzer Str.  38, 01187, Dresden, Germany}

\date{\today}



\begin{abstract}
Recently, the field of strongly correlated electrons has begun an intense search for a correlation induced topological insulating phase. An example is the quadratic band touching point which arises in a checkerboard lattice at half-filling, and in  the presence of interactions gives rise to topological Mott insulators. In this work, we perform a mean-field theory computation to show that such a system shows instability to topological insulating phases even away from half-filling (chemical potential $\mu  =  0 $). The interaction parameters consist of on-site repulsion ($ U $), nearest-neighbor  repulsion ($ V $), and a next-nearest-neighbor correlated hopping ($ t_ c $). The $t_ c$ interaction  originates  from  strong  Coulomb  repulsion. By tuning the  values of these parameters, we obtain the desired topological phase spanning the area around $(V  =  0  ,\,  \mu  =  0)$, extending to regions with $(V>0,\,\mu=0)$ and $(V>0,\,\mu>0)$. This extends the realm of current experimental efforts to find these topological phases.
\end{abstract}

\pacs{73.43.-f, 73.43.Nq, 71.10.Fd}


\maketitle

%\tableofcontents

\section{Introduction}
Study of topological phases in condensed matter systems is one of the most active areas of research in recent times~\cite{hasan-kane}.
In conventional topological insulators, a combination of spin-orbit interactions and time-reversal symmetry gives rise to protected conducting states at edges/surfaces in spite of the presence of a bulk band gap (like an ordinary insulator). The focus has mostly been on noninteracting systems. In this work, we consider the proposal of inducing topological insulating phases in 2D materials through interactions, without the need for spin-orbit coupling or large intersite interactions~\cite{raghu,kai-sun,herbut,tsai,vafek,wu}. While bulk insulating gaps arise due to interactions, the topological nature is captured by the topologically protected edge states. We will dub them ``topological Mott insulators"~\cite{raghu}. The quantum anomalous Hall (QAH) effect, emerging as a 2D topological insulating phase, can be understood as a generalization of the quantum Hall effect for the spin-singlet case, which is an integer quantum Hall phase with gapless chiral currents at the edges, but realized in the absence of any external magnetic field. The QAH ground state breaks time reversal symmetry with unbroken lattice translational symmetry and has a bulk insulating gap.
The quantum spin Hall (QSH) effect is the analogue of the QAH effect, but the gapless edge states are helical such that electrons with the opposite spins counter-propagate giving rise to spin currents (rather than charge currents). Also, the ground state does not break time reversal symmetry.
In the set-ups for realizing interaction-driven 2D topological phases proposed so far~\cite{kai-sun,herbut,tsai,vafek,wu}, electron-electron interactions at a quadratic band crossing point (QBCP) were considered, as parabolically touching bands have a finite density of states in 2D.

QBCPs can arise on the checkerboard~\cite{kai-sun} (at $\frac{1}{2}$ filling), Kagome~\cite{kai-sun} (at $\frac{1}{3}$ filling),  and  Lieb~\cite{tsai} lattices. The spin-singlet $d$-density wave (DDW) state in checkerboard lattice is the same as the QAH phase discussed in Ref.~\onlinecite{kai-sun,dona}. This is because when the diagonal hopping terms are modulated right at the lattice level, this in effect gives us the $d_{xy} + i\,d_{x^2-y^2}$ phase on the square lattice, which is the QAH phase involving the time-reversal symmetry breaking current loops. The corresponding triplet variety is the QSH phase. We also note that the QSH phase has the same energy as the QAH phase, and hence cannot be distinguished at the mean-field level.

%There is no intrinsic reason why the QAH phase is realizable with interactions only when the underlying non-interacting Fermi surface is point-like, that is, at a QBCP. 
There is no intrinsic reason why the interaction-induced QAH phase should not exist for a generic $\mu$ if the lattice and the interactions are of the right kind.
In the previously studied models, the interaction parameters considerd were: on-site repulsion ($ U $), and nearest-neighbor  repulsion ($ V $). We generalize this by including a pair-hopping (or correlated hopping)~\cite{Nayak:2002}, denoted by an interaction strength $t_c$, as this term is known to favor DDW/QAH ordering~\citep{Nayak:2000}. The next-nearest-neighbor correlated hopping ($ t_ c $) originates from strong  Coulomb  repulsion. In this paper, our goal is to show that there is a QAH phase on the checkerboard lattice (at least in mean-field theory) for a chemical potential ($\mu$) that does not exactly correspond to a Fermi point at quadratic band touching, and in fact it continuously connects/extends to the regions of $(V=0,\,\mu=0)$ and $(V>0,\, \mu=0)$, given that we tune $U$ and $t_c$ to some optimal values.
Our study considers instabilities among QAH, charge-density wave (CDW) and spin-density wave (SDW) phases.
We employ a mean-field theory approach by minimizing the free energy involving the possible competing phases in order to study the effect of chemical potentials away from the QBCP.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The checkerboard lattice model}
\label{model}

\begin{figure}[t]
\centering
\includegraphics[width =0.5  \linewidth]{lattice}
\caption{\label{lattice} The checkerboard lattice, with the nearest-neighbor hopping of amplitude $t$ denoted by the green lines. The two next-nearest-neighbor hopping amplitudes $\frac{t_{diag}}{2}$ and $-\frac{t_{diag}}{2}$ are shown by dashed brown and dashed yellow lines respectively.}
\end{figure}


We consider the Hamiltonian of Ref.~\onlinecite{wu}, illustrated in Fig.~\ref{lattice}:
\begin{widetext}
\begin{align}
H &= H_{0}  + H_{int}\,,\nn
H_{0}  & = - \mu  \sum \limits_{\sigma} \sum \limits_{i,j }
 c_{i,j; \sigma}^\dagger\, c_{ i,j;\sigma}
-   t  \sum \limits_{\sigma} \sum \limits_{ i,j } 
\left (  c_{ i,j; \sigma}^\dagger\, c_{ i+1,j;\sigma} +  c_{ i,j; \sigma}^\dagger\, c_{ i,j+1;\sigma} 
+ c_{ i,j; \sigma}^\dagger\, c_{ i-1,j;\sigma} +  c_{ i,j; \sigma}^\dagger\, c_{ i,j-1;\sigma} 
\right ) \nn
& \hspace{0.5 cm}  + \frac{t_{diag}}{2}  \sum \limits_{\sigma} \sum \limits_{i,j } (-1)^{i+j} \left (
c_{i,j; \sigma}^\dagger\, c_{ i+1,j+1;\sigma} + c_{i,j; \sigma}^\dagger\, c_{ i-1,j-1;\sigma}
- c_{i,j; \sigma}^\dagger\, c_{ i+1,j-1;\sigma} - c_{i,j; \sigma}^\dagger\, c_{ i-1,j+1;\sigma}
\right ),\nn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
H_{int}  & = U  \sum \limits_{ i,j} n_{i,j;\uparrow}\, n_{i,j;\downarrow}
+ 
V \sum \limits_\sigma \sum \limits_{ \langle m, m' \rangle} n_{m;\sigma}\, n_{m';\sigma } 
-t_c  \sum \limits_{\sigma } \sum \limits_{\substack{ \langle m, m'  \rangle,     \langle m',  m'' \rangle \\ m\neq m''}}
 c_{m; \sigma}^\dagger\, c_{m';\sigma} \, c_{m';\sigma}^\dagger   c_{m'';\sigma}  \,,
 \label{ham1}
\end{align}
\end{widetext}
with $\big \lbrace m=(i,j),m'=(i',j'),m'' =(i'',j'') \big \rbrace $ and $(\sigma, \sigma')$ denoting the site and spin indices respectively. The nearest-neighbor pairs hop with strength $t$, while the next-nearest neighbors are connected by diagonal bonds with hopping strength $t_{diag}$. We note that the diagonal hoppings give rise to the commensurate singlet $d_{xy}$-density wave ordering \cite{Nayak:2000}.
In the interaction terms, $U $ is the on-site repulsion, $V$ is the nearest-neighbor repulsion, and $t_c$ is the next-nearest-neighbor correlated hopping. The symbol $ \langle m, m' \rangle$ indicates nearest-neighbor pairs. The next-nearest-neighbor correlated hopping occurs when an electron hops from $m''$ to $m'$ when $m'$, in turn, is vacated by an electron hopping to $m$. For the non-interacting part ($H_0$), the QBCP appears at half-filling ($\mu=0$), when the non-interacting electrons have a finite density of states but lack a Fermi surface. To simplify the notation, $t$ and the nearest-neigbhor bond length $a$ are set to be units of energy and length.

Our aim is to reduce the Hamiltonian into a form including various ordered phases~\cite{Nayak:2000, Nayak:2002} as follows~\footnote{Nayak and Pivovarov~\cite{Nayak:2002} solved the mean-field phase diagram arising from correlated hopping on a bilayer square lattice, while we are interested in a single layer checkerboard lattice.}:
\begin{widetext}
\begin{align}
H_{mf}=&\int{[d\mathbf{k}] \left( \epsilon_\mathbf{k} -\mu \right)\, c^\dagger_{\mathbf{k};\sigma}\, c_{\mathbf{k};\sigma}} + \int{[d\mathbf{k}]\,\tilde{\epsilon}_\mathbf{k}\,(c^\dagger_{\mathbf{k}+\mathbf{Q}  ;\sigma}   \,c_{\mathbf{k};\sigma}+h.c.)}\nonumber \\
&-g_{ddw}\iint{[d\mathbf{k}\,][d\mathbf{k}']\,f_\mathbf{k}\,f_\mathbf{k}'
\left[c^\dagger_{\mathbf{k}+\mathbf{Q};\sigma}\, c_{\mathbf{k};\sigma} \right]
\left[ c^\dagger_{\mathbf{k}';\sigma'}  \, c_{\mathbf{k}'+\mathbf{Q};\sigma'}\right] }
%%%%%%%%%
-g_{ddw}\iint{[d\mathbf{k}]  \, [d\mathbf{k}']\, f_\mathbf{k} \,f_\mathbf{k}'\left[c^{\dagger}_{\mathbf{k}+\mathbf{Q};\alpha}\,\sigma_z^{\alpha\beta}\,c_{\mathbf{k}; \beta} \right]
\left[ c^{\dagger}_{\mathbf{k}';\gamma}\, \sigma_z^{\gamma\delta}\, c_{\mathbf{k}'+\mathbf{Q}; \delta}\right ]}\nonumber \\
%%%%%%%%%%%%%%%%%%%%%%%
&-g_{sdw}\iint{[d\mathbf{k}]\,[d\mathbf{k}']\,\left [c^{\dagger}_{\mathbf{k}+\mathbf{Q};\alpha}\, \sigma_z^{\alpha\beta}
\,c_{\mathbf{k} ;  \beta}\right]
\left[c^{\dagger}_{\mathbf{k}'; \gamma}\,\sigma_z^{\gamma\delta}\,c_{\mathbf{k}'+\mathbf{Q}; \delta} \right ]}
%%%%%%%%%%%%%%%%%%%%%%%%
 -g_{dsc}\iint{[d\mathbf{k}]\,[d\mathbf{k}']\,f_\mathbf{k}\,f_\mathbf{k}'
\left[c^\dagger_{\mathbf{k}\uparrow}c_{-\mathbf{k}\downarrow}\right]
\left[ c^\dagger_{-\mathbf{k}'\uparrow}\, c_{\mathbf{k}'\downarrow} \right]}
%%%%%%%%%%%%%%%%%%%
\nn &  -g_{cdw}\iint{[d\mathbf{k}]\,[d\mathbf{k}'] 
\left[c^\dagger_{\mathbf{k}+\mathbf{Q};\sigma}\, c_{\mathbf{k};\sigma} \right]
\left[ c^\dagger_{\mathbf{k}';\sigma'}\, c_{\mathbf{k}'+\mathbf{Q};\sigma'}\right] }
\,,\nn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mathbf Q = & \, (\pi,\pi)\,, \quad f_\mathbf{k} = \cos k_x-\cos k_y\,,\quad [d\mathbf{k}] =\frac{dk_x\, dk_y}{\pi^2}\,,
\label{ham2}
\end{align}
\end{widetext}
where the indices $\sigma,\alpha,\beta,\gamma,\delta$ are spin-indices and summation over repeated indices is implied. The free fermionic dispersion on a square lattice is $\epsilon_{\mathbf{k}}=-2 \,t\left(\cos k_x + \cos k_y \right )$, while $\tilde{\epsilon}_\mathbf{k}=-2\,t_{diag} \,\sin k_x\, \sin k_y$ is the energy dispersion for the $d_{xy}$ phase emerging from a checkerboard lattice with $t_{diag}$ as the diagonal hopping strength on adjacent plaquettes. The third, fourth, fifth, sixth and seventh terms on the RHS represent singlet DDW, triplet DDW, SDW, $d$-wave superconducting (DSC) and CDW phases respectively. The couplings are given by~\cite{Nayak:2002}:
\begin{align}
& g_{ddw}=8\,V+24\, t_c\,, \quad g_{sdw}=2\,U \,, \nn
& g_{dsc}=12\,t_c-8\,V\,,\quad g_{cdw} = 16\,V+24\,t_c -2\,U
\,.
\end{align}
Since the order parameters condense at the wave vector $\mathbf Q= (\pi, \pi) $, we use the reduced BZ (RBZ) by folding the full 2D Brillouin zone (BZ). The RBZ is  defined  in  terms  of  the  rotated  coordinates: $k_x' =\frac{k_x+k_y}{\sqrt 2}\,,\quad
k_y' =\frac{k_x -k_y}{\sqrt 2}$, where $k_x',\,k_y' \in \left [ -\frac{\pi}{\sqrt 2}, \,\frac{\pi}{\sqrt 2}\right ]$. 

Here, we will consider only two competing phases -- QAH (or DDW) and SDW. We will assume that the CDW phase is suppressed by a large enough $U$, which is true for the parameter regime when $g_{cdw} \leq 0$
~\footnote{In other words, we will restrict to the regime where CDW is not energetically favorable and therefore ruled out.}. 
We will also leave out DSC as our aim is to study the phase diagram away from the parameters leading to the familiar high-$T_c$ cuprate phase diagrams.


The SDW and the singlet $d_{x^2-y^2}$ order parameters can be expressed as: 
\begin{align}
\label{defn}
&\phi_{\text{sdw}} = g_{a}\int [d\mathbf{k}]
\left [ \langle c^\dagger_{\mathbf{k}+\mathbf{Q}; \uparrow}\, c_{\mathbf{k}; \uparrow} \rangle - \langle c^\dagger_{\mathbf{k}+\mathbf{Q}; \downarrow}\, c_{\mathbf{k}; \downarrow} \rangle    \right ]\,,\nn
&\phi_{\text{ddw}}^s = g_{b} \int [d\mathbf{k}]\,f_{\mathbf{k}}\,\langle 
c^\dagger_{  \mathbf{k}+{ \mathbf{Q} };\sigma  }
\,c_{\mathbf{k};\sigma}\rangle \,.
\end{align}
The QAH order parameter is decomposed in momentum space just like a DDW and looks like:
%\begin{equation}
$	i\, g_b\, f_{\mathbf{k}}\,\langle c^\dagger_{\mathbf{k}+\mathbf{Q};\sigma}\,c_{\mathbf{k};\sigma}\rangle\,.$
%\end{equation}

To derive a mean-field theory, it is convenient to take the
Fourier transform of the Hamiltonian in Eq.~(\ref{ham1}) and regroup the terms. To do so, first let us Fourier transform $H_{int}$ to obtain~\cite{Nayak:2000}:
\begin{widetext}
\begin{align}
H_{int}  & = \pi^2\, \sum \limits_{\sigma,\sigma'}\int   [d\vec k_1]    \, [d\vec k_2]\, [d\vec k_3]\, [d\vec k_4]\, 
\delta ( \vec k_1+ \vec k_3- \vec k_2 -\vec k_4 ) \, 
c^\dagger _{\vec k_1;\sigma} \, c _{\vec k_2; \sigma }\, c^\dagger _{\vec k_ 3;\sigma' } \, c _{\vec k_4 ;\sigma'}
[\,  V  \left \lbrace   \cos ( k^ x _3 - k^ x_4 )+\cos ( k^ y _3 - k^ y_4 ) \right \rbrace \nn
& \hspace{1.5 cm}
-   t_c \left \lbrace   \cos ( k^ x _1 - k^ x_4 )+\cos ( k^ y _1 - k^ y_4 ) +   2 \cos k ^x _1\,  \cos k ^x _4 +   2 \cos k ^y _1\,  \cos k ^y _4 \, \right \rbrace
\, ]  \,,
\end{align}
\end{widetext}
where $\pi^2 \int   [d\vec k]   \delta ( \vec k) =1 \,.$
~\footnote{As an example, we have demonstrated how to extract the DDW contribution from the  interaction  part  in  the supplementary material, following Ref.~\onlinecite{foglio}.}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



  
\subsection{Triplet DDW}
First we consider the triplet $d_{x^2-y^2}$ ordered phase~\cite{Nayak:2000, Nayak:2002}, such that the Hamiltonian can be written as:
\begin{align}
H^c_{mf}= & -g_c \int [d\mathbf{k}]\,[d\mathbf{k}']\,f_\mathbf{k}\, f_\mathbf{k'}
\left [ c^{\dagger}_{\mathbf{k}+\mathbf{Q}; \alpha}\,\sigma_z^{\alpha\beta}\,c_{\mathbf{k};\beta} \right ]\nn  
& \hspace{ 3 cm} \times  \left [ c^{\dagger}_{\mathbf{k}'; \gamma}  \,\sigma_z^{\gamma\delta} \, c_{\mathbf{k}'+\mathbf{Q};\delta}  \right ]\,,
\end{align}
where $g_c=g_{ddw}=8\,V+24\,t_c$. 
Expanding the spin indices above, we get:
\begin{widetext}
\begin{align}
H^c_{mf} &=-g_c\int  [d\mathbf{k}]  \, [d\mathbf{k}']\,f_\mathbf{k} \,f_\mathbf{k}'\left  [ c^{\dagger}_{\mathbf{k}+\mathbf{Q} ;\uparrow }  \,c_{\mathbf{k};\uparrow } - c^{\dagger}_{\mathbf{k} +\mathbf{Q};\downarrow} \,c_{\mathbf{k};\downarrow}  \right ] \left  [ c^{\dagger}_{\mathbf{k'} ;\uparrow }  \,c_{\mathbf{k'}+\mathbf{Q};\uparrow } - c^{\dagger}_{\mathbf{k'} ;\downarrow} \,c_{\mathbf{k'}+\mathbf{Q};\downarrow}  \right ]\nn
%%%%%%%%%%%
&=-g_c\int  [d\mathbf{k}]\,[d\mathbf{k}']\,f_\mathbf{k}\, f_\mathbf{k}'
\,\Big [ c^{\dagger}_{\mathbf{k}+\mathbf{Q} ;\uparrow }  \,c_{\mathbf{k};\uparrow }\,c^{\dagger}_{\mathbf{k'} ;\uparrow }  \,c_{\mathbf{k'} +\mathbf{Q};\uparrow }
 -  c^{\dagger}_{\mathbf{k}+\mathbf{Q} ;\uparrow }  \,c_{\mathbf{k};\uparrow }\,c^{\dagger}_{\mathbf{k'} ;\downarrow }  \,c_{\mathbf{k'} +\mathbf{Q};\downarrow }
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nn & \hspace{ 3.8 cm} 
-c^{\dagger}_{\mathbf{k}+\mathbf{Q} ;\downarrow }  \,c_{\mathbf{k};\downarrow }\,c^{\dagger}_{\mathbf{k'} ;\uparrow }  \,c_{\mathbf{k'} +\mathbf{Q};\uparrow }
 +  c^{\dagger}_{\mathbf{k}+\mathbf{Q} ;\downarrow }  \,c_{\mathbf{k};\downarrow }\,c^{\dagger}_{\mathbf{k'} ;\downarrow }  \,c_{\mathbf{k'} +\mathbf{Q};\downarrow }
\Big ] \,.
\end{align}
We define the two mean-field order parameters:
\begin{align}
&\phi_c^ {\uparrow} =  g_{c}\int{[d\mathbf{k}]\,f_{\mathbf{k}}\,\langle c^\dagger_{\mathbf{k}+\mathbf{Q};\uparrow}}\,c_{\mathbf{k};\uparrow}\rangle\,, \quad
\phi_c^{\downarrow} = g_{c}\int{[d\mathbf{k}]\,f_{\mathbf{k}}\,\langle c^\dagger_{\mathbf{k}+\mathbf{Q};\downarrow}}\, c_{\mathbf{k};\downarrow}\rangle\,,
\end{align}
and expand the four-fermion operators using these to obtain the mean-field Hamiltonian: 
\begin{align}
H_{mf}^ c =  -\int [d\mathbf{k}]& \Big [ \phi_c^\uparrow\, f_\mathbf{k}\,c_{\mathbf k; \uparrow}^\dagger
\, c_{\mathbf k+\mathbf{Q}; \uparrow}+\phi_c^{\uparrow*}\,f_\mathbf{k}\,c_{\mathbf k+\mathbf{Q}; \uparrow}^\dagger\, c_{\mathbf k; \uparrow}  - \frac{\phi_c^\uparrow\, \phi_c^{\uparrow*}}{g_c}
%%%%%%%%%%%%%
 -\phi_c^\uparrow\, f_\mathbf{k}\, c_{\mathbf k; \downarrow}^\dagger\,c_{\mathbf k+\mathbf{Q}; \downarrow}-\phi_c^{\downarrow*}\,f_\mathbf{k}\,c_{\mathbf k+\mathbf{Q}; \uparrow}^\dagger\,c_{\mathbf k; \uparrow}  + \frac{\phi_c^\uparrow  \,\phi_c^{\downarrow*}}{g_c}\nn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
&-\phi_c^\downarrow \,f_\mathbf{k}\, c_{\mathbf k; \uparrow}^\dagger\, c_{\mathbf k+\mathbf{Q}; \uparrow}-\phi_c^{\uparrow*}\,f_\mathbf{k}\, c_{\mathbf k+\mathbf{Q}; \downarrow}^\dagger\,c_{\mathbf k; \downarrow}  + \frac{\phi_c^{\uparrow*}\,\phi_c^{\downarrow}}{g_c}
%%%%%%%%%%%%%%%%%%%%
 + \phi_c^\downarrow \,f_\mathbf{k}\,c_{\mathbf k; \downarrow}^\dagger\,c_{\mathbf k+\mathbf{Q}; \downarrow}+\phi_c^{\downarrow*}\,f_\mathbf{k}\, c_{\mathbf k+\mathbf{Q}; \downarrow}^\dagger\,c_{\mathbf k; \downarrow}  - \frac{\phi_c^\downarrow\, \phi_c^{\downarrow*}}{g_c}  \Big ]\,.
\end{align}
We will now choose $\phi_c^\uparrow=i\,\phi_c$, and $\phi_c ^\downarrow=-i\,\phi_c$, where $\phi_c \in \mathbb{R} $, as this choice gives spin currents of equal magnitude (QAH state). The mean-field Hamiltonian then takes the form:
%\begin{widetext}
\begin{align}
H_{mf}^ c &=\int [d{\mathbf{k}}]\, \psi^\dagger_{\mathbf{k}}\,  \begin{pmatrix}
  \epsilon_{\mathbf{k}}-\mu &\tilde{\epsilon}_\mathbf{k}+2\,i \,\phi_c\, f_\mathbf{k} & 0 & 0\\
  \tilde{\epsilon}_\mathbf{k}-2\,i\, \phi_c\, f_\mathbf{k} & -\epsilon_{\mathbf{k}}-\mu & 0 & 0\\
  0 & 0 &\epsilon_{\mathbf{k}}-\mu & \tilde{\epsilon}_\mathbf{k}-2\,i\,\phi_c\,f_\mathbf{k}\\
  0 & 0 & \tilde{\epsilon}_\mathbf{k}+2\,i\,\phi_cf_\mathbf{k} &  -\epsilon_{\mathbf{k}} -\mu\\
 \end{pmatrix} \,   \psi_{\mathbf{k}}\,
 + \frac{4\,\phi_c^2}{g_c}\,,\nn
\psi^\dagger_{\mathbf{k}}  & =\left (c^\dagger_{\mathbf{k}; \uparrow}, c^\dagger_{\mathbf{k}+\mathbf{Q}; \uparrow}, c^\dagger_{\mathbf{k}; \downarrow}, c^\dagger_{\mathbf{k}+\mathbf{Q}; \downarrow}  \right ) .
\end{align}
\end{widetext}


\subsection{SDW and singlet DDW}


Using the expressions in Eq.~(\ref{defn}) for the SDW and the singlet $d_{x^2-y^2}$ order parameters,
where the corresponding couplings are $g_a=g_{sdw}=2\,U$ and $g_b=g_{ddw}=8\,V+24\,t_c  \,$,
one can find the mean-field Hamiltonian for these order parameters in the manner outlined for triplet DDW.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Phases from free energy minimization}
\label{phase}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t!]
\centering
%\subfigure[]{\includegraphics[width = 0.99 \columnwidth]{phase_diagram_smooth_fix_U_tc_0_00}} \\
\subfigure[]{\includegraphics[width = 0.99 \columnwidth]{phase_diagram_smooth_fix_U_2018-4-17_9-37-35_tc_0_020_td_0_75}} \\
\subfigure[]{\includegraphics[width = 0.99\columnwidth]{phase_diagram_smooth_fix_U_2018-4-17_9-37-47_tc_0_050_td_0_75}}
\caption{\label{fig1} The panels show the phase diagrams obtained for different values of $t_c$, in the $\mu-V$ plane. The QAH phase exists near $(\mu=0, \, V=0)$ region in the last panel. We have set $t_{diag}=0.75$ and $U=1.0$. The values of $t_c$ are $ \{ 0.02,0.05\}$ for the successive panels in increasing order. The ranges for $V$ are such that the CWD phase can be neglected.
All the parameters are in units with $t=1$.}
\end{figure}


\begin{figure}[t!]
\centering
%\subfigure[]
{\includegraphics[width =0.99  \linewidth]{phase_diagram_smooth_fix_mu_2018-6-14_15-29-1_mu_0_1_td_0_75}}
\caption{\label{fig2} Phase diagram for $\mu=0.1$, $t_{diag}=0.75$, $t_c =0.02$ in the $U-V$ plane for the allowed region in which the CDW phase can be neglected.}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


At the mean-field level, singlet DDW (or QAH) and triplet DDW (or QSH) turn out to have the same energies and hence we cannot distinguish between them. Therefore, we will determine the phase diagram by considering two order paremeters: one for SDW, and the other for singlet DDW.
For notational simplicity, we will use $\phi_a$ and $\phi_b$ to denote these two order parameters respectively. 
The mean-field Hamiltonian, including the above two phases, is given by:
\begin{widetext}
\begin{align}
H_{mf}^{ab} &=\int [d{\mathbf{k}}]\, \psi^\dagger_{\mathbf{k}}\,
\left [\, h(\mathbf{k})-\mu  \, \right ]
 \psi_{\mathbf{k}}
  + \frac{4\,|\phi_a|^2}{g_a} + \frac{4\,|\phi_b|^2}{g_b} \,,
 \quad \psi^\dagger_{\mathbf{k}}   =\left (c^\dagger_{\mathbf{k}; \uparrow}, c^\dagger_{\mathbf{k}+\mathbf{Q}; \uparrow}, c^\dagger_{\mathbf{k}; \downarrow}, c^\dagger_{\mathbf{k}+\mathbf{Q}; \downarrow}  \right ), \nn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  h(\mathbf{k})&=
  \begin{pmatrix}
  \epsilon_{\mathbf{k}}  &\tilde{\epsilon}_\mathbf{k}-2\,\phi_{a}-2\,i\,\phi_bf_\mathbf{k} & 0 & 0\\
  \tilde{\epsilon}_\mathbf{k}-2\,\phi_{a}+2\,i\,\phi_bf_\mathbf{k}  & -\epsilon_{\mathbf{k}}  & 0 & 0\\
  0 & 0 &\epsilon_{\mathbf{k}}  & 2\, \phi_{a}-2\,i\,\phi_bf_\mathbf{k} +\tilde{\epsilon}_\mathbf{k}\\
  0 & 0 & 2\,\phi_{a}+2\,i\,\phi_bf_\mathbf{k} +\tilde{\epsilon}_\mathbf{k} &  -\epsilon_{\mathbf{k}}  \\
 \end{pmatrix} \,. 
\end{align}
\end{widetext}
Let us assume for simplicity that $\phi_a$ and $\phi_b$ are real. Diagonalizing $h(\mathbf{k})$, the energy eigenvalues are found to be:
\begin{align}
E^1_\mathbf{k} & = \sqrt{  \epsilon_\mathbf{k}^2+\tilde{\epsilon}_\mathbf{k}^2+4\,\phi_a^2 +4\,\tilde{\epsilon}_\mathbf{k}\,\phi_a+4\,\phi_b^2\, f_\mathbf{k}^2} \,,\nn
E^2_\mathbf{k} & =  \sqrt{\epsilon_\mathbf{k}^2+\tilde{\epsilon}_\mathbf{k}^2+4\,\phi_a^2 -4\,\tilde{\epsilon}_\mathbf{k}\,\phi_a+ 4\, \phi_b^2 \, f_\mathbf{k}^2} \,,\nn
E^3_\mathbf{k} & = -E^1_\mathbf{k} \,,\quad
E^4_\mathbf{k} = - E^2_\mathbf{k} \,.
\end{align}
The ground state energy at zero temperature (same as free energy at $T=0$) becomes 
\begin{align}
F = \sum\limits_{n=1}^4 \int{[d\mathbf{k}]\,\,E_\mathbf{k}^n \,\theta(\mu-E_\mathbf{k}^n)} +\frac{4\,|\phi_a|^2}{g_a} + \frac{4\,|\phi_b|^2}{g_b}  \,,
\end{align}
where the sum over $n$ runs over four bands. For a given set of parameters, we only expect one ordered state. If one finds more than one ordered state, then the one with the lower free energy should dominate. Hence for each ordered parameter, one can minimize the free energy to find a self consistent equation. For example, for the SDW state, the self consistent equation becomes 
\begin{align}
& \frac{8\,\phi_a}{g_a} = - \frac{\partial}{ \partial \phi_a}
\sum\limits_{n=1}^4  \int{[d\mathbf{k}]\, E_\mathbf{k}^n \,\,\theta(\mu-E_\mathbf{k}^n)}\,.
\end{align}
Similarly one can derive self consistency equations for the other phase as well by minimizing the free energy energy. We choose units such that $t=1$.

We note that the SDW vertex depends only on $U$, whereas the DDW vertex depends only on $V$ and $t_c$. Therefore, DDW phase will be favored by increasing the values of $V$ and $t_c$. Clearly, if we want DDW phase to exist around $V=0$, we need to reach an optimum value of $t_c$. This is what our results show.


Fig.~\ref{fig1} shows the phase diagrams for $t_{diag}=0.75$ and $U =1.0$, such that the QAH phase appears around $(V=0,\,\mu=0)$ region by increasing $t_c$ to a nonzero optimum value, and extends continuously into the regions with $(V=0,\,\mu>0)$ and $(V>0,\,\mu>0)$. We have shown the ranges for $V$ for which we are allowed to neglect the CDW phase. 
Fig.~\ref{fig2} is shown to emphasize that the QAH phase can indeed exist near $V=0 $ for a nonzero value of $\mu$. In this case, $t_c=0.02$ and only the allowed region for neglecting CDW vertex is shown in the phase diagram.
%For completeness, we also show a phase diagram for $t_{\diag}=0$ in Fig.~\ref{fig4}.
The energy bands are shown in Figs.~\ref{fig3} and \ref{fig4}, which indicate that interactions open up gaps at the quadratic band touching point. We note that for the QAH phase, the band opening is such that we have $E_{\mathbf k}^1 = E_{\mathbf k}^2$ and $E_{\mathbf k}^3 = E_{\mathbf k}^4$, where $E_{\mathbf k}^1 =- E_{\mathbf k}^3$. In other words, the QAH phase is characterised by two doubly degenerate bands which are negative of each other, similar to the non-interacting Hamiltonian energies. On the contrary, for the SDW bands, there is no degeneracy. We note that when SDW appears at $\mu=0$ region, two of the bands still still touch other -- a gap appears only at higher values of $\mu$. On the contrary, when QAH state appears at $\mu =0$, a gap appears between the positive and negative energy bands, each remaining doubly degenerate. Around $(V=0,\,\mu=0)$, a higher value of $t_c$ thus enables this gap-opening, making the QAH state feasible in that region. 

Our numerical results show that QAH/DDW phase can exist for nonzero $\mu$ (away from QBCP) in the interaction-driven scenario, both for $V =0$ and $V>0$. Furthermore, an optimum $t_c$ value allows QAH phase to exist around $(V=0,\,\mu=0)$. Hence, this extends the realm of current experimental investigations to find these topological phases.



\begin{figure*}[t!]
\centering
\subfigure[]
{\includegraphics[width =0.23  \linewidth]{GXMG_U_0_00_V_0_00_mu_0_00_t_1_00_td_0_75_tc_0_00_phase_b}}
%\quad
\subfigure[]
{\includegraphics[width=0.23 \linewidth]{GXMG_U_1_00_V_0_00_mu_0_00_t_1_00_td_0_75_tc_0_02}}
%\quad
\subfigure[]
{\includegraphics[width =0.23 \linewidth]{GXMG_U_1_00_V_0_04_mu_0_00_t_1_00_td_0_75_tc_0_02}}
%\quad
\subfigure[]
{\includegraphics[width=0.23 \linewidth]{GXMG_U_1_00_V_0_02_mu_0_00_t_1_00_td_0_75_tc_0_05_phase_b}}
%\quad
\subfigure[]
{\includegraphics[width=0.23 \linewidth]{GXMG_U_1_00_V_0_01_mu_1_00_t_1_00_td_0_75_tc_0_02}}
%\quad
\subfigure[]
{\includegraphics[width=0.23 \linewidth]{GXMG_U_1_00_V_0_01_mu_1_50_t_1_00_td_0_75_tc_0_02}}\quad
\subfigure[]
{\includegraphics[width=0.23 \linewidth]{GXMG_U_1_00_V_0_03_mu_1_00_t_1_00_td_0_75_tc_0_02_phase_b}}
%\quad
\subfigure[]
{\includegraphics[width=0.23 \linewidth]
{GXMG_U_1_00_V_0_00_mu_0_00_t_1_00_td_0_75_tc_0_05_phase_b}}
\caption{\label{fig3} The four energy bands for (a) $t_{diag}=0.75,  \, U=0,\, V=0,\,t_c=0, \, \mu=0$ (non-interacting case); (b) $t_{diag}=0.75,  \, U=1,\, V=0,\,t_c=0.02 , \, \mu=0 $; (c) $t_{diag}=0.75, \, U=1,\, V=0.04,\,t_c=0.02 , \, \mu=0 $; (d) $t_{diag}=0.75, \, U=1,\, V=0.02,\,t_c=0.05 , \, \mu=0 $; (e) $t_{diag}=0.75, \, U=1,\, V=0.01,\,t_c=0.02 , \, \mu=1.0 $; (f) $t_{diag}=0.75, \, U=1,\, V=0.01,\,t_c=0.02 , \, \mu=1.5 $; (g) $t_{diag}=0.75, \, U=1,\, V=0.03,\,t_c=0.02 , \, \mu=1.0 $; (h) $t_{diag}=0.75, \, U=1,\, V=0,\,t_c=0.05 , \, \mu=0 $. As usual, we use the convention: $\Gamma = (0,0), \, X = ( -\pi/2 , \pi/2),\, M = ( 0, \pi )$. The panels showing only two energy bands actually involve each band being doubly degenerate.}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}[t!]
\centering
\subfigure[]
{\includegraphics[width =0.3 \linewidth]{surfaceE_U_0_00_V_0_00_mu_0_00_t_1_00_td_0_75_tc_0_00_phase_b}}
\quad
\subfigure[]
{\includegraphics[width=0.3 \linewidth]{surfaceE_U_1_00_V_0_04_mu_0_00_t_1_00_td_0_75_tc_0_02}}
\quad
\subfigure[]
{\includegraphics[width=0.3 \linewidth]{surfaceE_U_1_00_V_0_01_mu_1_00_t_1_00_td_0_75_tc_0_02}}
\quad
\subfigure[]
{\includegraphics[width=0.3 \linewidth]{surfaceE_U_1_00_V_0_01_mu_1_50_t_1_00_td_0_75_tc_0_02}}
\quad
\subfigure[]
{\includegraphics[width=0.3 \linewidth]{surfaceE_U_1_00_V_0_00_mu_0_00_t_1_00_td_0_75_tc_0_05_phase_b}}
\quad
\subfigure[]
{\includegraphics[width=0.3 \linewidth]{surfaceE_U_1_00_V_0_03_mu_1_00_t_1_00_td_0_75_tc_0_02_phase_b}}
\caption{\label{fig4} The four energy bands for (a) $t_{diag}=0.75,  \, U=0,\, V=0,\,t_c=0, \, \mu=0$ (non-interacting case); (b) $t_{diag}=0.75, \, U=1,\, V=0.04,\,t_c=0.02 , \, \mu=0 $; (c) $t_{diag}=0.75, \, U=1,\, V=0.01,\,t_c=0.02 , \, \mu=1.0 $; (d) $t_{diag}=0.75, \, U=1,\, V=0.01,\,t_c=0.02 , \, \mu=1.5 $;  (e) $t_{diag}=0.75, \, U=1,\, V=0,\,t_c=0.05 , \, \mu=0 $; (f) $t_{diag}=0.75, \, U=1,\, V=0.03,\,t_c=0.02 , \, \mu=1.0 $. The panels showing only two energy bands actually involve each band being doubly degenerate.}
\end{figure*}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}
\label{conclude}

%The importance of the correlated hopping term has been already mentioned by Hubbard[20]. Later Hirsch[21]pointedoutthatthistermmayberel-
%evantinexplaningthesuperconductingpropertiesofstron
%glycorrelatedelectrons. 
%20. HubbardJ.,Proc.R.Soc.LondonA,1963,276,238;doi:10.1098/rspa.1963.0204.
%21. HirschJ.E., PhysicaC,1989,158,236;doi:10.1016/0921-4534(89)90225-6.
%
%virtue of the Coulomb interaction be-
%tween the electrons. 


We have shown by our numerically derived mean-field phase diagrams that the QAH (or DDW) state appears in a generic doping range without fine tuning in the presence of interactions of the right kind. Our interaction terms include correlated hopping, which essentially originates from strong local Coulomb repulsion. In future work, one can study the effect of disorder for the finite chemical potential case. 

Experimental investigations of systems showing QBCPs are just starting~\cite{Armitage}. Since it is experimentally challenging to tune exactly at the point of zero chemical potential, our work shows that the experimental explorations with extended realms can access topological Mott phases.






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Acknowledgments}
We acknowledge Sumanta Tewari for suggesting the problem. We thank Girish Sharma for collaboration in the initial stages of the project.
We also thank Matthias Punk, Nyayabanta Swain, Vito Scarola and Hoi Hui for fruitful discussions. Lastly, we express our deepest gratitude to Michael J. Lawler for carefully going through the manuscript and suggesting improvements.








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{draft.bib}


\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
@article{Armitage,
  title={Dielectric anomalies and interactions in the three-dimensional quadratic band touching Luttinger semimetal Pr 2 Ir 2 O 7},
  author={Cheng, Bing and Ohtsuki, T and Chaudhuri, Dipanjan and Nakatsuji, S and Lippmaa, Mikk and Armitage, NP},
  journal={Nature communications},
  volume={8},
  number={1},
  pages={2097},
  year={2017},
  publisher={Nature Publishing Group}
}

@article{hasan-kane,
  title = {Colloquium: Topological insulators},
  author = {Hasan, M. Z. and Kane, C. L.},
  journal = {Rev. Mod. Phys.},
  volume = {82},
  issue = {4},
  pages = {3045--3067},
  numpages = {0},
  year = {2010},
  month = {Nov},
  publisher = {American Physical Society},
  doi = {10.1103/RevModPhys.82.3045},
  url = {https://link.aps.org/doi/10.1103/RevModPhys.82.3045}
}

@article{tsai,
  author={Wei-Feng Tsai and Chen Fang and Hong Yao and Jiangping Hu},
  title={Interaction-driven topological and nematic phases on the Lieb lattice},
  journal={New Journal of Physics},
  volume={17},
  number={5},
  pages={055016},
  url={http://stacks.iop.org/1367-2630/17/i=5/a=055016},
  year={2015},
  abstract={We show that topological states are often developed in two-dimensional semimetals with quadratic band crossing points (BCPs) by electron–electron interactions. To illustrate this, we construct a concrete model with the BCP on an extended Lieb lattice and investigate the interaction-driven topological instabilities. We find that the BCP is marginally unstable against infinitesimal repulsions. Depending on the interaction strengths, topological quantum anomalous/spin Hall, charge nematic, and nematic-spin-nematic phases develop separately. Possible physical realizations of quadratic BCPs are provided.}
}

@article{herbut,
  title = {Occurrence of nematic, topological, and Berry phases when a flat and a parabolic band touch},
  author = {D\'ora, Bal\'azs and Herbut, Igor F. and Moessner, Roderich},
  journal = {Phys. Rev. B},
  volume = {90},
  issue = {4},
  pages = {045310},
  numpages = {5},
  year = {2014},
  month = {Jul},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.90.045310},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.90.045310}
}


@article{vafek,
  title = {Renormalization group study of interaction-driven quantum anomalous Hall and quantum spin Hall phases in quadratic band crossing systems},
  author = {Murray, James M. and Vafek, Oskar},
  journal = {Phys. Rev. B},
  volume = {89},
  issue = {20},
  pages = {201110},
  numpages = {4},
  year = {2014},
  month = {May},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.89.201110},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.89.201110}
}


@article{foglio,
  title = {New approach to the theory of intermediate valence. I. General formulation},
  author = {Foglio, M. E. and Falicov, L. M.},
  journal = {Phys. Rev. B},
  volume = {20},
  issue = {11},
  pages = {4554--4559},
  numpages = {0},
  year = {1979},
  month = {Dec},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.20.4554},
  url = {http://link.aps.org/doi/10.1103/PhysRevB.20.4554}
}


@article{Nayak:2000,
  title = {Density-wave states of nonzero angular momentum},
  author = {Nayak, Chetan},
  journal = {Phys. Rev. B},
  volume = {62},
  issue = {8},
  pages = {4880--4889},
  numpages = {0},
  year = {2000},
  month = {Aug},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.62.4880},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.62.4880}
}


@article{Nayak:2002,
  title = {${d}_{{x}^{2}\ensuremath{-}{y}^{2}}$},
  author = {Nayak, Chetan and Pivovarov, Eugene},
  journal = {Phys. Rev. B},
  volume = {66},
  issue = {6},
  pages = {064508},
  numpages = {7},
  year = {2002},
  month = {Aug},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevB.66.064508},
  url = {https://link.aps.org/doi/10.1103/PhysRevB.66.064508}
}


@article{kai-sun,
  title = {Topological Insulators and Nematic Phases from Spontaneous Symmetry Breaking in 2D Fermi Systems with a Quadratic Band Crossing},
  author = {Sun, Kai and Yao, Hong and Fradkin, Eduardo and Kivelson, Steven A.},
  journal = {Phys. Rev. Lett.},
  volume = {103},
  issue = {4},
  pages = {046811},
  numpages = {4},
  year = {2009},
  month = {Jul},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.103.046811},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.103.046811}
}


@article{wu,
  title = {Diagnosis of Interaction-driven Topological Phase via Exact Diagonalization},
  author = {Wu, Han-Qing and He, Yuan-Yao and Fang, Chen and Meng, Zi Yang and Lu, Zhong-Yi},
  journal = {Phys. Rev. Lett.},
  volume = {117},
  issue = {6},
  pages = {066403},
  numpages = {6},
  year = {2016},
  month = {Aug},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.117.066403},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.117.066403}
}


@article{raghu,
  title = {Topological Mott Insulators},
  author = {Raghu, S. and Qi, Xiao-Liang and Honerkamp, C. and Zhang, Shou-Cheng},
  journal = {Phys. Rev. Lett.},
  volume = {100},
  issue = {15},
  pages = {156401},
  numpages = {4},
  year = {2008},
  month = {Apr},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.100.156401},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.100.156401}
}


